
# Table of Contents

1.  [fixations](#fixations)
    1.  [fixationCoder.py](#fixation-coder)
    2.  [coded<sub>data</sub>](#coded_data)
    3.  [irr](#orgce7e570)
2.  [questionnaires](#org5c85d08)
3.  [miscellaneous](#miscellaneous)
    1.  [weather](#weather)
    2.  [corrected time points](#corrected-time-points)

Prepares the coded data to be ready for analysis. Contains the final data sets.


<a id="fixations"></a>

# fixations

Subjects fixations coded with meanings.


<a id="fixation-coder"></a>

## fixationCoder.py

-   tbd


<a id="coded_data"></a>

## coded<sub>data</sub>

Directory for coded fixation log files

1.  Line endings

    Line endings seem to be affected by the OS.
    
    Using
    =dos2unix=([website](https://waterlan.home.xs4all.nl/dos2unix.html))
    to convert text files formats (mac/dos) to unix.
    
    Install the program running: `sudo apt install dos2unix`) and run from
    the directory of the text files:
    `${TEMPDIR} -iname '*.txt' -exec dos2unix {} \; -exec sed -i '$d' {} \;`
    (found [here](https://stackoverflow.com/a/30728753/7285920))


<a id="orgce7e570"></a>

## irr

-   tbd


<a id="org5c85d08"></a>

# questionnaires

Subjects answers in questionnaires.


<a id="miscellaneous"></a>

# miscellaneous

Some external and reconstructed data


<a id="weather"></a>

## weather

Data for the weather was found [here](https://www.wetterkontor.de/de/wetter/deutschland/rueckblick.asp?id=209&datum0=16.10.2019&datum1=23.12.2019&jr=2020&mo=7&datum=18-10-2019%09&t=4&part=0#tabelle).


<a id="corrected-time-points"></a>

## corrected time points

Extracted the starting points for each participant from the recruiting platform
and personal notes. Necessary for correcting the wrong time settings on the
mobile eye tracking device.

