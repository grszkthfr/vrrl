
# Table of Contents

1.  [weather](#weather)
2.  [corrected time points](#corrected-time-points)

Some external and reconstructed data


<a id="weather"></a>

# weather

Data for the weather was found [here](https://www.wetterkontor.de/de/wetter/deutschland/rueckblick.asp?id=209&datum0=16.10.2019&datum1=23.12.2019&jr=2020&mo=7&datum=18-10-2019%09&t=4&part=0#tabelle).


<a id="corrected-time-points"></a>

# corrected time points

Extracted the starting points for each participant from the recruiting platform
and personal notes. Necessary for correcting the wrong time settings on the
mobile eye tracking device.

