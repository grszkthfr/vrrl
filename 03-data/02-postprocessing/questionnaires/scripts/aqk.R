# Missings checken und durch Mittelwerte aller uebrigen Probanden ersetzen
check_aqk <- function(x) {
    nan <- apply(x,1,function(x) { sum(is.na(x)) })
    if (sum(nan!=0)!=0) {
        print(paste("Missing values in",sum(nan!=0),"case(s)"))
        print(nan[nan!=0])
    } else {
        print("AQK: All cases complete")
    }

    # Missings durch MW aller Probanden ersetzen
    for (i in 1:ncol(x)) {
        if (sum(is.na(x[,i]))!=0) {
            x[is.na(x[,i]),i] <- round(mean(x[,i],na.rm=TRUE))
        }
    }

    return(x)
}

score_aqk <- function(data)
{
    # scale should be 1-4
    data <- data.frame(data)
    # AQ-K
    data <- check_aqk(data)
    # Items 1,3,5,6,7,9,10,11,14,16,17,18,20,22,23,24,26,28,31,32,33 umpolen (4-item).
    data[,c(1,3,5,6,7,9,10,11,14,16,17,18,20,22,23,24,26,28,31,32,33)] <- 5-data[,c(1,3,5,6,7,9,10,11,14,16,17,18,20,22,23,24,26,28,31,32,33)]
    # Items rekodieren: Chris choice, no/less agreement: 1/2 -> 0, full/strong agreement: 3/4 -> 1)
    matrix_recode <- matrix(as.numeric(data[,1:ncol(data)]>=3),nrow=nrow(data))

    data.frame(
        aqk_interaction = rowSums(matrix_recode[,c(1,7,8,10,11,13,14,20,24,28,31)]),    # Soziale Interaktion und Spontaneitaet
        aqk_imagination2 = rowSums(matrix_recode[,c(3,5,6,9,16,17,18,22,23,26,32,33)]),  # Fantasie und Vorstellungsvermoegen
        aqk_communication = rowSums(matrix_recode[,c(2,4,12,15,19,21,25,27,29,30)]),      # Kommunikation und Reziprozitaet
        aqk_sumscore = rowSums(matrix_recode)
        )                                      # Gesamtsummenwert (AQ-K-Score) zwischen 0 und 33

}

