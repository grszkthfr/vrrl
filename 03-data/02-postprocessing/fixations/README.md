
# Table of Contents

1.  [fixationCoder.py](#fixation-coder)
2.  [coded<sub>data</sub>](#coded_data)
    1.  [Line endings](#line-endings)
3.  [irr](#org0f2c02d)

Subjects fixations coded with meanings.


<a id="fixation-coder"></a>

# fixationCoder.py

-   tbd


<a id="coded_data"></a>

# coded<sub>data</sub>

Directory for coded fixation log files


<a id="line-endings"></a>

## Line endings

Line endings seem to be affected by the OS.

Using
=dos2unix=([website](https://waterlan.home.xs4all.nl/dos2unix.html))
to convert text files formats (mac/dos) to unix.

Install the program running: `sudo apt install dos2unix`) and run from
the directory of the text files:
`${TEMPDIR} -iname '*.txt' -exec dos2unix {} \; -exec sed -i '$d' {} \;`
(found [here](https://stackoverflow.com/a/30728753/7285920))


<a id="org0f2c02d"></a>

# irr

-   tbd

