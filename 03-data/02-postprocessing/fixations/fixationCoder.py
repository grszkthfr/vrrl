#!/usr/bin/python
# by Jonas Großekathöfer, 2019
"""
Just good enough script to code fixations for each subject for each recorded
frame.
To code any subject or video, please specifiy the according log below in the
settings section and have fun coding. :-)
"""
from os import path  # replace with pathlib
import os
import glob
import csv
import cv2
import random
import time
# import numpy as np


# TODO use pandas and merge with previous data

#############################################################################
################# Settings  #################################################
#############################################################################

# keys
KEY_FIX_PERSON = 112 #p 49         # key "1" to code a fixation_id on a person
KEY_FIX_OBJECT = 111 #o 50         # key "2" to code a fixation_id on a object
KEY_FIX_BACKGROUND = 98 #b 51     # key "3"

KEY_PERSON_IN_SCENE = 115 #s 112   # key "p"

KEY_NO_FIXATION = 109 #m 110       # key "0" to code a fixation_id on a person
KEY_DELETE_FRAME = 100      # key "d" to delete previuos frame,. go back
KEY_NEXT_FRAME = 32         # key "space" to write line, go to next frame
KEY_EXIT = 27

INSTRUCITION_FIXATION = 'choose from p, o, b, m, d'
INSTRUCITION_SCENE = 'toggle with *s*'

#############################################################################


def here(file_name='.here'):

    """
    Works not on mac!
    from: https://stackoverflow.com/questions/37427683/python-search-for-a-file-in-current-directory-and-all-its-parents
    adapted to work with here-package in R
    """

    current_dir = os.getcwd()  # search starts here
    # print(current_dir)

    while True:
        file_list = os.listdir(current_dir)
        parent_dir = os.path.dirname(current_dir)
        if file_name in file_list:
            # print('File Exists in: ', current_dir)
            break

        else:
            if current_dir == parent_dir:  # if dir is root dir
                # print('File not found')
                break
            else:
                current_dir = parent_dir

    return current_dir


def writeLine(out_file, row):

    # writes an extra blank line on macOS :-(
    with open(out_file, 'a', newline='') as save_file:
        writer = csv.writer(save_file, delimiter='\t')  # tab separated
        if os.stat(out_file).st_size == 0:  # if file is empty, insert header
            writer.writerow((
                'subject_id', 'trial_id', 'setting_id', 'frame_id',
                'fixation_id', 'person_in_scene'))

        writer.writerow(row)


def deleteLine(out_file):

    with open(out_file, 'r') as save_file:

        reader = csv.reader(save_file, delimiter='\t')

        new_save_file = list(reader)

        new_save_file = new_save_file[:-1]  # truncat last entry

    # write trials, without deleted one
    with open(out_file, 'w', newline='') as save_file:
        writer = csv.writer(save_file, delimiter='\t')  # tab separated

        for row in new_save_file:
            writer.writerow(row)


def resizeImage(image, output_height=840):

    scale = output_height/image.shape[0]

    width = int(image.shape[1] * scale)
    height = int(image.shape[0] * scale)

    # resize image
    resized_image = cv2.resize(image, (width, height), interpolation = cv2.INTER_AREA)

    return resized_image

def addFrameInformation(image, subject_id, setting_id, trial_id):

    font = cv2.FONT_HERSHEY_PLAIN
    height, width, channels = image.shape

    x_1 = width-450
    y_1 = 0

    x_2 = x_1 + 400
    y_2 = 15

    # information
    cv2.putText(
        image,
        f'subject_id: {subject_id}, setting_id: {setting_id}, trial_id: {trial_id}',
        (10, 50),
        font, 1, (99, 46, 255))

    return image


def updateCoding(image, frame_id, fixation_id, person_in_scene):

    font = cv2.FONT_HERSHEY_PLAIN
    height, width, channels = image.shape

    # frame information box
    cv2.rectangle(
        image,
        (0, height-60), (430, height),
        (0, 0, 0), -1)

    # frame_id
    cv2.putText(
        image,
        'frame_id: ' + frame_id, (2, height-40),
        font, 1, (255, 255, 255))

    # person_in_scene
    cv2.putText(
        image,
        'person_in_scene: ' + person_in_scene, (2, height-25),
        font, 1, (255, 255, 255))
    if person_in_scene == 'person_in_scene':
        # frame information box
        color_scene = (0,255,0)

    else:
        color_scene = (0,0,255)

    # color code scene
    cv2.rectangle(
        image,
        (2, 2), (width-2, height-2),
        color_scene, 1)

    # fixtiaon_id
    cv2.putText(
        image,
        'current fixation_id: ' + fixation_id, (2, height-12),
        font, 1, (255, 255, 255))

    # update window
    cv2.imshow('frame', image)


def togglePersonInScene(person_in_scene):

    if person_in_scene == 'no_person_in_scene':
        person_in_scene = 'person_in_scene'
    else:
        person_in_scene = 'no_person_in_scene'

    return person_in_scene


def takeInput(frame_id, frame_img, fixation_id, person_in_scene):

    next_frame = True
    while next_frame:

        cv2.imshow('frame', frame_img)

        k = cv2.waitKey(33)

        # if missing:
        #     counter += 1
        #     break

        if k == KEY_EXIT:  # Esc

            next_frame = False
            cv2.destroyAllWindows()
            break

        elif k == -1:  # normally -1 returned, so don't print it
            continue

        # toggle person_in_scene
        elif k == KEY_PERSON_IN_SCENE:

            person_in_scene = togglePersonInScene(person_in_scene)
            updateCoding(frame_img, frame_id, fixation_id,
                                   person_in_scene)

        # fixations on person
        elif k == KEY_FIX_PERSON:  # #1
            fixation_id = 'person'
            person_in_scene = 'person_in_scene'
            updateCoding(frame_img, frame_id, fixation_id,
                                   person_in_scene)

        # fixations on object
        elif k == KEY_FIX_OBJECT:  # #2
            fixation_id = 'object'
            updateCoding(frame_img, frame_id, fixation_id,
                                   person_in_scene)

        # fixations on background
        elif k == KEY_FIX_BACKGROUND:  # #3
            fixation_id = 'background'
            updateCoding(frame_img, frame_id, fixation_id,
                                   person_in_scene)

        # special keys
        elif k == KEY_NO_FIXATION:  # n
            fixation_id = 'missing_fixation'
            updateCoding(frame_img, frame_id, fixation_id,
                                   person_in_scene)

        elif k == KEY_DELETE_FRAME:  # d
            fixation_id = 'delete'
            updateCoding(frame_img, frame_id, fixation_id,
                                   person_in_scene)

        elif k == KEY_NEXT_FRAME:  # space

            # only break while loop if valid input
            if fixation_id != INSTRUCITION_FIXATION and person_in_scene != INSTRUCITION_SCENE:

                return next_frame, fixation_id, person_in_scene

                # break

        else:  # else print key value

            fixation_id = INSTRUCITION_FIXATION
            updateCoding(frame_img, frame_id, fixation_id,
                                   person_in_scene)

            # https://stackoverflow.com/questions/14494101/using-other-keys-for-the-waitkey-function-of-opencv
            # print(k)
            # print(repr(chr(k % 256)))


    return next_frame, '', ''


def writeLog(subject_id, setting_id, trial_id, frame_id, fixation_id, person_in_scene):

    out_file = path.join("03-data", "02-postprocessing", "fixations", "coded_data", "fixations_" + str(subject_id).zfill(2) + ".txt")

    if fixation_id != 'delete':
        move = 1
        line = [subject_id, trial_id, setting_id,
                frame_id, fixation_id, person_in_scene]
        writeLine(out_file, line)

        print(f'coded {frame_id}\n\tfixation_id:\t\t{fixation_id}\n\tperson_in_scene:\t{person_in_scene}')

    elif fixation_id == 'delete':
        move = -1
        fixation_id = 'deleted'
        deleteLine(out_file)

        print('deleted last frame_id')

    return move


def extractInfo(frame):

    path_l = path.normpath(frame).split(os.sep)

    subject_id = path_l[-5].split("_")[1]
    setting_id = path_l[-4]
    trial_id = path_l[-2]
    frame_id = path_l[-1]

    return subject_id, setting_id, trial_id, frame_id


def filterFixations(all_frames):

    coded_data_dir = path.join(
        "03-data",
        "02-postprocessing",
        "fixations",
        "coded_data")

    frames_to_code = []
    prev_coded_data = []
    coded_frames_ids = []

    # previous coded frames
    prev_coded_data.extend(
        glob.glob(
            os.path.join(coded_data_dir, "*.txt")))

    for coded_data_file in prev_coded_data:
        with open(coded_data_file, "r") as f:
            next(f)  # skip header row
            for line in f:
                ids = line.split("\t")[0:4]
                str_ids = ''.join(ids)
                coded_frames_ids.append(str_ids)

    counter=0
    frames_ids = []
    for frame in all_frames:
        ids = frame.split(os.sep)[-5:]

        ids.pop(2)
        ids[0] = ids[0].split("_")[1]  # remove'subject_'
        ids[1], ids[2] = ids[2], ids[1]  # switch position trial_id setting_id
        str_ids = ''.join(ids)
        # frames_ids.append(str_ids)
        if str_ids in coded_frames_ids:
            # print(f"Already coded: {ids}")
            counter+=1
        else:
            frames_to_code.append(all_frames[counter])
            counter+=1

    print(f"LOG:\tA total of {len(frames_to_code)} ({int(len(frames_to_code)/len(all_frames)*100)}%) needs to be coded.")

    return frames_to_code


# def mergePreprocessed(log):

#     subject_id = str(log.subject_id[0]).zfill(2)
#     prot_file = path.join(
#         '02-experiment', 'vr', 'prot',
#         'prot_' + subject_id + '.csv')
#     prot = pd.read_csv(prot_file)

#     merged_df = pd.merge(
#         log,
#         prot,
#         on=('subject_id', 'setting_id', 'trial_id'),
#         how='inner')

#     merged_df.subject_id = subject_id

#     return merged_df


def showFrames(frames):

    counter = 0
    next_frame = True
    start_stopwatch = time.time()


    while counter < len(frames) and next_frame:

        frame = frames[counter]

        subject_id, setting_id, trial_id, frame_id = extractInfo(frame)
        frame_img = cv2.imread(frame)

        frame_img = resizeImage(frame_img)

        frame_img = addFrameInformation(frame_img, subject_id, setting_id, trial_id)

        # for first frame show "instructions"
        if counter == 0:

            fixation_id = INSTRUCITION_FIXATION
            person_in_scene = INSTRUCITION_SCENE

        updateCoding(
            frame_img, frame_id,
            fixation_id, person_in_scene)

        cv2.imshow('frame', frame_img)

        # returns only valid input
        next_frame, fixation_id, person_in_scene = takeInput(frame_id, frame_img, fixation_id, person_in_scene)

        # write only to log, if frame was coded, but not when aborted
        if next_frame is True:
            move = writeLog(subject_id, setting_id, trial_id, frame_id,
                            fixation_id, person_in_scene)
            counter += move # +1 when write -1 when delete

            if counter < 0: # if deleted last coded frame, break
                break

        # next_frame is False only when actively [q]uit
        else:
            end_stopwatch = time.time()
            duration = (end_stopwatch - start_stopwatch)/60
            if counter > 0:
                print(f'Stopwatch:\tOn avarage, {int(counter/duration)} frames/minute.')

            break


def run(frames_dir):

    all_frames = []
    all_frames.extend(
        glob.glob(
            os.path.join(
                frames_dir,
                'subject_*',
                '**',  # rl AND vr
                'frames',
                '[!v]*',  # each location but not validation!
                '*.png'),
                recursive=True))

    # random.shuffle(all_frames)

    print(f"LOG:\tFound a total of {len(all_frames)} in preprocessed_data.")
    frames = filterFixations(all_frames)
    # on macOS frames are not sorted
    frames.sort()

    # https://stackoverflow.com/questions/16815194/how-to-resize-window-in-opencv2-python
    showFrames(frames)


# log file
LOG_FILE = path.join('03-data', '01-preprocessing', 'preprocessed_data')


run(LOG_FILE)
# run()
