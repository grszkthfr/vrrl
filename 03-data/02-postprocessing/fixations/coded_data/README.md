
# Table of Contents

1.  [Line endings](#line-endings)

Directory for coded fixation log files


<a id="line-endings"></a>

# Line endings

Line endings seem to be affected by the OS.

Using
=dos2unix=([website](https://waterlan.home.xs4all.nl/dos2unix.html))
to convert text files formats (mac/dos) to unix.

Install the program running: `sudo apt install dos2unix`) and run from
the directory of the text files:
`${TEMPDIR} -iname '*.txt' -exec dos2unix {} \; -exec sed -i '$d' {} \;`
(found [here](https://stackoverflow.com/a/30728753/7285920))

