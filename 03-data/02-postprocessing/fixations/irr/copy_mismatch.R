# copies all mismatchs from orgiginal destination to new a folder and renames
# frame to include the disagreement in file name

frames_fixation <-
    here(
        "03-data", "01-preprocessing", "preprocessed_data",
        str_c("subject", mismatch_fixations$subject_id, sep = "_"),
        mismatch_fixations$setting_id,
        "frames",
        mismatch_fixations$trial_id,
        mismatch_fixations$frame_id)

irr_fixation <-
    here(
        "03-data", "02-postprocessing", "fixations", "irr", "mismatch_frames",
        str_c(
            "fixation",
            "subject", mismatch_fixations$subject_id,
            mismatch_fixations$setting_id,
            mismatch_fixations$trial_id,
            str_c("i", mismatch_fixations$fixation_id_i, sep = "-"),
            str_c("c", mismatch_fixations$fixation_id_c, sep = "-"),
            mismatch_fixations$frame_id,
            sep = "_"))

# copy the files to the new folder
file_copy(path = frames_fixation, new_path = irr_fixation)


# pfade <-
frames_person <-
    here(
        "03-data", "01-preprocessing", "preprocessed_data",
        str_c("subject", mismatch_persons$subject_id, sep = "_"),
        mismatch_persons$setting_id,
        "frames",
        mismatch_persons$trial_id,
        mismatch_persons$frame_id)

irr_person <-
    here(
        "03-data", "02-postprocessing", "fixations", "irr", "mismatch_frames",
        str_c(
            "person",
            "subject", mismatch_persons$subject_id,
            mismatch_persons$setting_id,
            mismatch_persons$trial_id,
            str_c("i", mismatch_persons$person_in_scene_i, sep = "-"),
            str_c("c", mismatch_persons$person_in_scene_c, sep = "-"),
            mismatch_persons$frame_id,
            sep = "_"))

# copy the files to the new folder
file_copy(path = frames_person, new_path = irr_person)
