
# Table of Contents

1.  [Scripts](#scripts)

Qualitative checks of measurement quality. Calculate distance between measured
fixation point and actual fixation point. Potentially use a drift correction.


<a id="scripts"></a>

# Scripts

-   `make_empty_val-onsets.R` make tables for relevant onsets
    -   *vr* known /frame<sub>id</sub>/s for fixations
        -   00450.png: top, 00300.png: right, 00600.png: left
-   `getDistance.py` opens all relevant validation frames from
    *preprocessed<sub>data</sub>/*
    -   checks only for existing /validation<sub>XX.csv</sub>/s, never stop with a
        subject unfinished

