#!/usr/bin/python
# coding: utf-8

# by Jonas Großekathöfer, 2019

from pathlib import Path
import pandas as pd
import numpy as np
import cv2


def findValidationCoords(event,x,y,flags,param):
    global val_x, val_y

    if event == cv2.EVENT_LBUTTONDOWN:
        click, val_x , val_y = True, x, y
        print(f'LOG\tActual fixation set to {val_x}, {val_y}')


def getCoords(image_file, trial_id, out_file):

    image = cv2.imread(str(image_file))
    height, width, channels = image.shape
    cv2.putText(image, trial_id, (int(width/2)+10,height-12), cv2.FONT_HERSHEY_PLAIN, fontScale=8, color=(21,245,113), thickness=4)
    clone = image.copy()
    cv2.namedWindow('Validation Frame')
    cv2.setMouseCallback('Validation Frame', findValidationCoords)

    while(1):
        cv2.imshow('Validation Frame', image)
        k = cv2.waitKey(20) & 0xFF
        if k == 32: # space

            cv2.destroyWindow('Validatiaon Frame')        
            print(f'LOG\tCorrected Fixation coordinates are {val_x},{val_y}')

            save_image = cv2.circle(
                img=image, 
                center=(val_x, val_y), 
                radius=5, 
                color=(21,245,113), 
                thickness=-1)
            cv2.imwrite(filename=str(out_file), img=save_image)


            return val_x, val_y


def run(log_dir):

    print("<<< START DRAWING FIXATIONS INTO FRAMES >>>", log_dir.is_dir())
    onset_logs = list(log_dir.glob('*.csv'))

    # comment!
    for log in onset_logs:

        onsets = pd.read_csv(log, sep="\t")
        subject_id = str(onsets.subject_id[0]).zfill(2)

        validation_file = Path(
            '03-data', '03-quality_checks', 'validation',
            'corrected_fixations', f'validation_{subject_id}.txt')
        validation_file.parent.mkdir(exist_ok=True, parents=True)

        print(f'LOG\tFind validation frames for subject_{subject_id}.')

        # setting up for empty df
        cols = ['subject_id', 'setting_id', 'validation_id', 'trial_id', 'frame_id', 'corrected_x', 'corrected_y']
        log = []
        if not validation_file.is_file():
            for setting_id, validation_id, trial_id, frame_id in zip(
                    onsets.setting_id,
                    onsets.validation_id,
                    onsets.trial_id,
                    onsets.first_frame_id):

                if setting_id == 'vr':
                    every_nth = [0,10,20,30,40]

                if setting_id == 'rl':
                    every_nth = [0,5,10,15,20]

                for val_frame_id in [n+frame_id for n in every_nth]:
                    if not pd.isna(val_frame_id):

                        val_frame_id = int(val_frame_id)
                        frame_file = str(val_frame_id).zfill(5) + '.png'

                        frame_path = Path(
                            '03-data',
                            '01-preprocessing',
                            'preprocessed_data',
                            'subject_' + subject_id,
                            setting_id,
                            'frames',
                            'val' + validation_id,
                            frame_file)
                        
                        val_frame_path = Path(
                            '03-data', 
                            '03-quality_checks',
                            'validation', 
                            'validation_frames', 
                            f'subject_{subject_id}',
                            f'val_{setting_id}_{validation_id}',
                            frame_file)

                        val_frame_path.parent.mkdir(
                            parents = True, 
                            exist_ok=True)

                        if frame_path.is_file():
                            print(f'LOG\tFound frame at {frame_path}')

                            corrected_x, corrected_y = getCoords(frame_path, trial_id, val_frame_path)

                        else:

                            print(f'LOG\tNo frame at {frame_path}')

                    else:
                        val_frame_id = 0
                        corrected_x, corrected_y = 0, 0

                    log.append(
                        [subject_id, setting_id, validation_id, trial_id,
                            val_frame_id, corrected_x, corrected_y])

            # build dataframe
            df_validation = pd.DataFrame(log, columns=cols)

            # save dataframe, does not overwrite
            df_validation.to_csv(validation_file, sep='\t', index=False)


onsets_dir = Path(
    '03-data', '03-quality_checks', 'validation',
    'position_onsets')

run(onsets_dir)
