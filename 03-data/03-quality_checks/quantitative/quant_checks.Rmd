---
title: "Notebook: Quantitative Checks"
author: "Jonas Großekathöfer"
date: "last update: `r format(Sys.time(), '%d %B, %Y, %H:%Mh')`"
output:
    html_notebook:
        toc: TRUE
        toc_float: TRUE
---

This Notebooks performs all kinds of quantitative checks for the *preprocessed 
data* in the worst possible way. No pictures only numbers and tables.

## Preparation

Load packages.

```{r packages}

library(here)
library(fs)
library(tidyverse)

source(here("03-data","scripts","read_vr_log.R"))
source(here("03-data","scripts","read_rl_log.R"))
source(here("03-data","scripts","check_valid_fixation.R"))

```

## Known Information

Some information is known, like how many subjects should appear overall
logfiles, how many different trials, etc. ...

```{r general}

total_subjects <- 44
total_trials <-  5
total_trial_parts <-  2
total_trial_duration <- 15  ## for one part (vr)/ half (rl)
total_setting_ids <-  2
FPS <- 5

```

We know also how many fixations in total there are per subject: 15 (seconds) * 2
(parts) * 5 (FPS) * 5 (locations). This is interesting if we want to see
the relative numbers (%) of missing fixations for example to identify subjects
with bad performance due to weahter or make up etc. ....

```{r total fixations per subject}

total_subject_id_fix <- total_trial_duration * total_trial_parts * FPS * total_trials
total_subject_id_fix

```

We know also how many fixations in total there are per trial: 15 (seconds) * 2
(parts) * 5 (FPS) * 5 (locations). This is interesting if we want to see
the relative numbers (%) of missing fixations for example to to identify trials
(locations/videos) with bad performance due to places in the shade or specific
video characteristics (weird encodings) etc. ...

```{r total fixations per trial}

total_trial_id_fix <- total_trial_duration * total_trials * total_trial_parts * total_subjects
total_trial_id_fix

```

```{r}

total_fix <- total_trial_duration * total_trials * total_trial_parts * total_subjects * total_trials
total_fix


```


We also know how many log files to expect for rl: 27 subjects * (5 locations + 2
validations).

```{r total rl log files}

total_rl_log_files <- total_subjects * (total_trials + 2)
total_rl_log_files

```


We also know how many log files to expect for vr: 27 subjects * ((5 locations *
2 parts) + 2 validations).

```{r total vr log files}

total_vr_log_files <- total_subjects * ((total_trials * total_trial_parts) + 2) ## for separate parts
total_vr_log_files

```

## Log files

How many log files do we find in ...

### RL

#### How many recorded log files?

```{r actual rl log files}

rl_log_files <-
    dir_ls(
        path(here(), "03-data", "01-preprocessing", "preprocessed_data"),
        glob = "**/rl/logs/**.txt",
        recurse = T)

n_rl_log_files <- length(rl_log_files)
n_rl_log_files

```

#### How many noted protocol files? (rl only)

```{r}

rl_prot_files <-
    dir_ls(
        path(here(), "02-experiment", "rl", "prot"),
        glob = "*.csv",
        recurse = T)

n_rl_prot_files <- length(rl_log_files)
n_rl_prot_files


```

This is exactly what we expected.

### VR

#### How many recorded log files?

```{r actual vr log files}

vr_log_files <-
    dir_ls(
        path(here(), "03-data", "01-preprocessing", "preprocessed_data"),
        glob = "**/vr/**.txt",
        recurse = T)

n_vr_log_files <- length(vr_log_files)
n_vr_log_files

```

This is exactly what we expected.

## Data quality

To get a better understanding of tracking quality let's have a look at the data.

```{r load log}

## load Data
rl_log <- read_rl_log()
vr_log <- read_vr_log()

```

### Subjects

How many different subjects are found in **all** log files?

```{r actual subjects}

n_subjects <- length(unique(c(rl_log$subject_id, vr_log$subject_id)))
n_subjects

```

This is exactly what we expected.

#### trial_id (Subjects)

##### RL

How many trials did every subject do in real life?

```{r actual trials per subject (rl)}

rl_log %>%
    group_by(subject_id) %>%
    summarise(
        n_trial_id = length(unique(trial_id))) %>%
    arrange(n_trial_id)

```

This is exactly what we expected, we *have* data for every participant at every
location (trial).

##### VR

How many trials did every subject do in virtual reality?

```{r actual trials per subject (vr)}

vr_log %>%
    group_by(subject_id) %>%
    summarise(n_trial_id = length(unique(trial_id))) %>%
    arrange(n_trial_id)

```

This is exactly what we expected, we *have* data for every participant at every
location (trial).

#### Fixations (Subjects)

#### RL

How many fixations are valid overall trials for each subject in rl?

```{r load weather}

log_weather <- read_tsv(
    here("03-data", "02-postprocessing", "miscellaneous", "weather.txt"),
    col_types = cols(
        date = col_date("%d-%m-%Y")))

```

```{r valid fixations per subject (rl)}

fix_sbj_rl <- 
    rl_log %>%
    left_join(
      log_weather %>%
      select(date, sun, avrgd_temperature), by="date") %>% 
    group_by(subject_id) %>%
    mutate(
        date = date[1],
        sun = sun[1],
        valid_fix = check_valid_fix(setting_id, fix_pos_x, fix_pos_y)) %>%
    summarise(
        date = date[1],
        hour = median(lubridate::hour(date_time)),
        sun = sun[1], 
        n_valid_fix = sum(valid_fix),
        p_valid_fix = round(n_valid_fix/total_subject_id_fix,2)) %>%
    arrange(n_valid_fix)
fix_sbj_rl

```

In a perfect world, with a perfect eye tracker we would expect
`r total_subject_id_fix` fixations per participant. However, the world is not
perfect, especially the real world. On average we have 
`r round(mean(fix_sbj_rl$p_valid_fix),2)*100`% valid (i.e. measuerd) fixations 
per participant ranging from `r round(min(fix_sbj_rl$p_valid_fix),2)*100`% to 
`r round(max(fix_sbj_rl$p_valid_fix),2)*100`%.

Additionally, the date and the weather ([data source](https://www.wetterkontor.de/de/wetter/deutschland/rueckblick.asp?id=209&datum0=16.10.2019&datum1=23.12.2019&jr=2020&mo=7&datum=18-10-2019%09&t=4&part=0#tabelle))
show clusterd performance differences, which might indicate that the sun plays a
role here. A correlation between sun hours and proportion of valid fixations
shows *r* = `r round(cor(fix_sbj_rl$sun, fix_sbj_rl$p_valid_fix),2)`. On top,
one might take the daytime into account, expecting worse performance during
midday.

#### VR

How many fixations are valid overall trials for each subject in vr?

```{r valid fixations per subject (vr)}

fix_sbj_vr <- 
    vr_log %>%
    group_by(subject_id) %>%
    mutate(
        valid_fix = check_valid_fix(setting_id, fix_pos_x, fix_pos_y)) %>%
    summarise(
        n_valid_fix = sum(valid_fix),
        p_valid_fix = round(n_valid_fix/total_subject_id_fix,2)) %>%
    arrange(n_valid_fix)
fix_sbj_vr

```

In VR the world we create seems to be more perfect, for an eye tracker at least.
On average we have `r round(mean(fix_sbj_vr$p_valid_fix),2)*100`% valid (i.e.
measuerd) fixations per participant ranging from 
`r round(min(fix_sbj_vr$p_valid_fix),2)*100`% to 
`r round(max(fix_sbj_vr$p_valid_fix),2)*100`%!

##### Samples (vr only, subjects)

For some frames there are multiple samples (fixations) in the output. Here we
get an idea how big the problem is.

```{r}

smp_sbj_vr <- 
    vr_log %>%
    filter(fix_pos_avrgd == 1) %>%
    group_by(subject_id) %>%
    summarise(
        n_avrgd = round(n()/(total_trials*total_trial_parts)),
        m_diff_fix_pos_x = round(mean(abs(fix_pos_diff_x))),
        m_diff_fix_pos_y = round(mean(abs(fix_pos_diff_y)))) %>%
    arrange(subject_id)
smp_sbj_vr

```

You can see, that on average `r round(mean(smp_sbj_vr$n_avrgd))` frames per
participant were recoreded twice. That is from a (theoretical) total of 750
frames per participant around 1%! Additionally, the maximal distance between
those doubled-measured fixations is 
`r round(max(smp_sbj_vr$m_diff_fix_pos_y, smp_sbj_vr$m_diff_fix_pos_x))` 
pixel, on averate the distance is 
`r round(mean(c(smp_sbj_vr$m_diff_fix_pos_y, smp_sbj_vr$m_diff_fix_pos_x)))`.

### Trials

How many different trials can be found in **all** log files?

```{r actual n trials (rl)}

n_trials <-
    length(unique(c(rl_log$trial_id, vr_log$trial_id)))
n_trials

```

As expected.

#### subject_id (Trials)

##### RL

How many subjects *did* every trial do in real life?

```{r actual n subjects per trial (rl)}

rl_log %>%
    group_by(trial_id) %>%
    summarise(n_subject_id = length(unique(subject_id))) %>%
    arrange(n_subject_id)

```

As expected.

##### VR

How many subjects *did not* do every trial in virtual reality (per part)?

```{r actual subjects per trial (vr)}

vr_log %>%
    group_by(trial_id, trial_part) %>%
    summarise(n_subject_id = length(unique(subject_id))) %>%
    arrange(n_subject_id)

```

Here we see, that 4 parts were not recored from 5 participants.

#### Fixations (Trials)

##### RL

How many fixations are valid overall subjects for each trial in rl?

```{r valid fixations per trial (rl)}

fix_trl_rl <- 
    rl_log %>%
    group_by(trial_id) %>%
    mutate(
        valid_fix = check_valid_fix(setting_id, fix_pos_x, fix_pos_y)) %>%
    summarise(
        n_valid_fix = sum(valid_fix),
        p_valid_fix = round(n_valid_fix/total_trial_id_fix,2)) %>%
    arrange(n_valid_fix)
fix_trl_rl

```

Here we see, that the best tracking quality with `r
max(fix_trl_rl$p_valid_fix)*100`% valid fixations was at the
[Backöfele](https://www.google.de/maps/dir/49.7922765,9.9294201/@49.7921181,9.92941,20.25z).

##### VR

How many fixations are valid overall subjects for each trial in vr?

```{r valid fixations per trial (vr)}

fix_trl_vr <- 
    vr_log %>%
    group_by(trial_id) %>%
    mutate(
        valid_fix = check_valid_fix(setting_id, fix_pos_x, fix_pos_y)) %>%
    summarise(
        n_valid_fix = sum(valid_fix),
        p_valid_fix = round(n_valid_fix/total_trial_id_fix,2)) %>%
    arrange(n_valid_fix)
fix_trl_vr

```

We see no differences between locations in VR.

##### Samples  (vr only, Trials)

For some frames there are multiple fixations in the output.

```{r}

vr_log %>%
    filter(fix_pos_avrgd == 1) %>%
    group_by(trial_id, trial_part) %>%
    summarise(n_avrgd = round(n()/total_subjects))

```

Here we see that multiple samples are not more or less frequent according to
special videos (i.e. locations).

### Measures

#### RL

How many (subject x trials) measures are available in rl?

```{r actual fixations per subjectXtrial (rl)}

rl_log %>%
    group_by(subject_id, trial_id) %>%
    summarise(n_samples = n()) %>%
    arrange(n_samples)

```

And how many are valid fixations?

```{r valid fixations per subjectXtrial (rl)}

fix_trlXsbj_rl <- 
    rl_log %>%
    group_by(subject_id, trial_id) %>%
    mutate(
        valid_fix = check_valid_fix(setting_id, fix_pos_x, fix_pos_y)) %>%
    summarise(
        n_valid_fix = sum(valid_fix),
        p_valid_fix = round(n_valid_fix/(total_subject_id_fix/total_trials),2)) %>% # == (total_trial_id_fix/n_subjects)
    arrange(n_valid_fix)
fix_trlXsbj_rl

```

Here we can see, why loc and reg perform poorly, there is even one participant
with *no* valid fixation! However, note the range from 
`r min(fix_trlXsbj_rl$p_valid_fix)*100`% to 
`r max(fix_trlXsbj_rl$p_valid_fix)*100`% valid fixations.

#### VR

How many (subject x trials) measures are available in vr?

```{r actual fixations per subjectXtrial (vr)}

vr_log %>%
    group_by(subject_id, trial_id) %>%
    summarise(
        n_samples = n()) %>%
    arrange(n_samples)

```

And how many are valid fixations?

```{r valid fixations per subjectXtrial (vr)}

fix_trlXsbj_vr <- 
    vr_log %>%
    # for some frames we have multiple measure, which get averaged
    select(
        subject_id, trial_id, setting_id, trial_part, frame_id,
        fix_pos_x, fix_pos_y) %>%
    distinct() %>%
    group_by(subject_id, trial_id) %>%
    mutate(
        valid_fix = check_valid_fix(setting_id, fix_pos_x, fix_pos_y)) %>%
    summarise(
        n_valid_fix = sum(valid_fix),
        p_valid_fix = round(n_valid_fix/(total_subject_id_fix/total_trials),2)) %>% # (total_trial_id_fix/n_subjects)
    arrange(n_valid_fix)
fix_trlXsbj_vr

```

We can see which participants missed which location.
