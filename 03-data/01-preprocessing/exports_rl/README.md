-   *metrics<sub>exports</sub>*:
    -   exported (and processed by SMI BeGaze) metrics for each participant at
        each location.
    -   **NOTE:** Replace all wrong or broken file names of recordings and
        *&rsquo;Participant&rsquo;* (later: subject<sub>location</sub>) entries before preprocessing!
-   *video<sub>exports</sub>*:
    -   exported (and processed by SMI BeGaze) videos for each participant at
        each location.
    -   Videos are exported at 10 FPS, see **6. *Analysis* > *Gaze Replay***

