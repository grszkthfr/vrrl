
# Table of Contents

1.  [frames](#org9522279)
2.  [frames<sub>tmp</sub>](#orgf198477)
3.  [logs](#org65680f2)
4.  [Directory for preprocessed data](#directory-for-preprocessed-data)
5.  [Build a video from frames](#build-a-video-from-frames)

For each subject for *vr* and *rl* the following sub-folders exist


<a id="org9522279"></a>

# frames

fore each location a folder with the final (to be coded) frames with drawn
fixations (from logs), for trials (vs. validation) there should be 150 frames
per location (75 per folder in *vr*)


<a id="orgf198477"></a>

# frames<sub>tmp</sub>

for each location a folder with **all** frames from exported videos, **without**
fixation circle


<a id="org65680f2"></a>

# logs

for each location a text file with all relevant samples for relevant frames


<a id="directory-for-preprocessed-data"></a>

# Directory for preprocessed data

Fixations ready for post processing.


<a id="build-a-video-from-frames"></a>

# Build a video from frames

Requirements: ffmpeg, then enter the directory the video should play and
insert in the CLI of your choice:

`ffmpeg -framerate 5 -pattern_type glob -i '*.png' ../gas.mp4`

