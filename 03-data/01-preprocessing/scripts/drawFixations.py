#!/usr/bin/python
# coding: utf-8

# by Jonas Großekathöfer, 2019

"""
TODO
"""

import os
from os import path  # TODO replace with pathlib
from pathlib import Path
import glob
import pandas as pd
import cv2 as cv2
import numpy as np
import here


# https://stackoverflow.com/a/34325723/7285920 never checked code
def printProgressBar(iteration, total, prefix='', suffix='', decimals=1,
                      length=100, fill='█'):
    """
    Call in a loop to create terminal progress bar
    @params:
        iteration   - Required  : current iteration (Int)
        total       - Required  : total iterations (Int)
        prefix      - Optional  : prefix string (Str)
        suffix      - Optional  : suffix string (Str)
        decimals    - Optional  : positive number of decimals in percent complete (Int)
        length      - Optional  : character length of bar (Int)
        fill        - Optional  : bar fill character (Str)
    """
    percent = ("{0:." + str(decimals) + "f}").format(100 * (iteration /
                                                            float(total)))
    filledLength = int(length * iteration // total)
    bar = fill * filledLength + '-' * (length - filledLength)
    print('\r%s |%s| %s%% %s' % (prefix, bar, percent, suffix), end = '\r')
    # Print New Line on Complete
    if iteration == total:
        print()


def selectFrames(log):

    # if log['setting_id'][0] == 'rl':
    #     # if 25 fps video: 750 frames, if 10 fps video 300 frames
    #     print('REAL LIFE')


    # elif log['setting_id'][0] == 'vr':
    #     # if frame saved every 200ms: 75 frames
    #     print('VIRTUAL REALITY')

    log_selection = log.dropna(subset=['frame_id'])
    log_selection = log_selection.reset_index(drop=True)

    return log_selection

#  does not do anything atm, because workflow changed. previous: draw fixation
#  delete the rest, now: draw fixation of image in new folder. TODO: delete
#  "frames_all" folder when arguement given
def deleteFrames(selected_frame_ids, all_frames_dir):

    count_delete = 0
    count_keep = 0

    for frame in glob.glob(path.join(all_frames_dir, '*.png')):

        frame_id = Path(frame).name

        if frame_id not in selected_frame_ids:
            # do nothing
            # os.remove(frame)
            count_delete += 1

        elif frame_id in selected_frame_ids:
            # do nothing
            count_keep += 1

        else:
            print(f"ERROR:\tFrame was neither deleted not kept!!!")

    return count_delete, count_keep


def drawFixation(frame, fixation_size, gaze_x, gaze_y):

    """
    Draws fixation at recored point and returns image for saving.
    """

    gaze_position = (int(gaze_x), int(gaze_y))  # 216

    # defaults by SMI
    # vr: hmd
    # img = cv2.circle(frame, gaze_position, 3, (0, 255, 255), 2)  # yellow
    # rl: glasses
    # img = cv2.circle(frame, gaze_position, 22, (255, 0, 255), 1)  # purple

    # fixation point of the size of 1 visual degree
    img = cv2.circle(frame, gaze_position, fixation_size, (99, 46, 255), 5)

    # img = cv2.flip(frame, 0)  # 0 = horizontal, 1 = vertical, -1 = both

    return img


def calculateFixationPoint(size_px, size_dg):

    one_dg_in_px = int(size_px/size_dg)

    return one_dg_in_px


def readFrame(frame_screenshot):

    """
    Check whether screenshot for the given path of the specific frame
    exists and return black image with warning when missing.
    """

    # check if file is missing
    if not os.path.isfile(frame_screenshot):
        # print(f"ERROR:\tFound no screenshot for frame {frame_screenshot.stem}!")
        img = np.zeros((840, 840, 3), np.uint8)

        # text
        cv2.putText(
            img,
            'MISSING SCREENSHOT!', (75, 378),
            cv2.FONT_HERSHEY_PLAIN, 2, (255, 255, 255))

        missing_screenshot = True

    else:

        missing_screenshot = False
        img = cv2.imread(str(frame_screenshot))  # cv2 can't handle Path

    return img, missing_screenshot


def run(data_dir, setting='**'):

    print("<<< START DRAWING FIXATIONS INTO FRAMES >>>", data_dir)

    # comment!
    for log_file in glob.glob(
        path.join(
            data_dir,
            'logs',
            '*.txt')):

        log_dir = Path(log_file).parent

        count_missing = 0

        # read log file
        log = pd.read_csv(log_file, sep='\t')
        subject_id = str(log.subject_id[0]).zfill(2)
        trial_id = log.trial_id[0]
        setting_id = log.setting_id[0]

        all_frames_dir = Path(log_dir.parent, 'frames_tmp', trial_id)
        output_dir = Path(log_dir.parent, 'frames', trial_id)
        os.makedirs(output_dir, exist_ok=True)

        # if dir not empty go to next
        if list(Path(output_dir).rglob('*')):
            print(f"LOG:\tDirectory for frames with fixations for subject {subject_id} at location in {trial_id} setting {setting_id} not empty.")
            continue

        print(f"LOG:\tStart drawing fixations for subject {subject_id} at location in {trial_id} setting {setting_id}")

        log_selection = selectFrames(log)
        print(f"LOG:\tSelected {len(log_selection)} frames from {len(log)} samples for drawing fixations.")

        frame_id_list = log_selection.frame_id.to_list()
        fix_pos_x_list = log_selection.fix_pos_x.to_list()
        fix_pos_y_list = log_selection.fix_pos_y.to_list()

        if setting_id == 'vr':
            max_w_px = 756
            max_h_px = 840
            max_w_dg = 100  # HTV fov

        if setting_id == 'rl':
            max_w_px = 1280
            max_h_px = 960
            max_w_dg = 60  # SMI camera fov

        # calculates relative pixel size for fixation marker to be 1 visual
        # degree
        fixation_point_size = calculateFixationPoint(max_w_px, max_w_dg)

        frame_n = 0
        for frame_id, fix_pos_x, fix_pos_y in zip(frame_id_list,
                                                  fix_pos_x_list,
                                                  fix_pos_y_list):

            in_frame_path = all_frames_dir.joinpath(frame_id)
            out_frame_path = output_dir.joinpath(frame_id)

            frame, missing_frame = readFrame(in_frame_path)

            missing_fixation = (0 > fix_pos_x >= max_w_px) and (0 > fix_pos_y >= max_h_px)

            # if no frame, or coords off frame dimensions count missing
            if missing_frame or missing_fixation:

                fix_pos_x = 9999
                fix_pos_y = 9999
                count_missing += 1


            frame = drawFixation(frame, fixation_point_size, fix_pos_x, fix_pos_y)
            # frame = addInfo(frame, subject_id, trial_id, setting_id)

            cv2.imwrite(str(out_frame_path), frame)

            printProgressBar(frame_n, len(frame_id_list))
            frame_n += 1

            # cv2.imshow('frame', frame)
            # k = cv2.waitKey(1000)
            # if k == 110:  # (n)ext frame
                # break

        count_delete, count_keep = deleteFrames(frame_id_list, all_frames_dir)

        print(f"\nLOG:\tFinished! Drawed fixations to {count_keep} frames, {count_missing} frames were missing, deleted {count_delete} frames.")
