#!/usr/bin/python
# coding: utf-8

# by Jonas Großekathöfer, 2019

"""

Find raw log from SMI, find onsets, find frames and bring all together to:
log with coords, frames with drawn fixations.

"""

from os import path  # TODO replace with pathlib
from pathlib import Path
# import subprocess
import pandas as pd
import numpy as np
import cv2 as cv2
from bisect import bisect_left

import extractLog
import drawFixations


def assignOnsets(list_frames, frame_rate, first_frame_time, first_frame_n, trial_duration, frame_rate_export=5):

    first_frame_time = int(first_frame_time)
    last_frame_time = first_frame_time + (trial_duration * 1000)
    total_frame_n = int(frame_rate * trial_duration)
    last_frame_n = first_frame_n + total_frame_n

    # Build sequence from first frame (frame_ms) till last frame (video
    # duration) in n_frames steps
    frame_time_smi = np.linspace(
        first_frame_time, last_frame_time,
        total_frame_n+1)  # in ms
    frame_time_smi = list(map(int, frame_time_smi))
    frame_time_smi.pop()

    # export only at 5fps, remove the rest
    export_nth_frame = int(frame_rate/frame_rate_export)  # frames per second, 60fps:

    # keep only frames from first till last frame_n
    selected_frames = list_frames[first_frame_n:first_frame_n+total_frame_n]
    # https://stackoverflow.com/questions/1403674/pythonic-way-to-return-list-of-every-nth-item-in-a-larger-list
    selected_frames = selected_frames[::export_nth_frame]

    # Create empty data frame
    df_frames = pd.DataFrame(
        columns=['frame_n', 'frame_rate', 'frame_id', 'frame_time_smi'])

    # Add sequence as column to data frame
    df_frames.frame_time_smi = list(map(int, frame_time_smi[::export_nth_frame]))
    df_frames.frame_rate = frame_rate
    df_frames.frame_id = list(map(path.basename, selected_frames))

    # Count all frames, starting at 1
    frame_n = range(1, total_frame_n+1)[::export_nth_frame]
    df_frames.frame_n = frame_n

    print(df_frames.head())

    return df_frames


def mergeLogs(df_frames, df_log):

    """
    Takes log for frames and finds nearest samples for each frame and returns complete log.
    """

    df_merged = df_log
    sample_time_smi = df_log.sample_time_smi.values.tolist()

    sample_n = list(range(1, len(df_log.index)+1))
    df_log['sample_n'] = sample_n

    # find nearest frame to sample
    for frame_time_smi, frame_n, frame_id in zip(
        df_frames.frame_time_smi,
        df_frames.frame_n,
        df_frames.frame_id):

        # return closest sample time
        sample_time, sample_id = takeClosest(sample_time_smi, frame_time_smi)

        # safe value for specific index
        df_merged.at[sample_id, 'frame_time_smi'] = frame_time_smi
        # positive values -> frame bevor sample, negative -> sample before frame
        df_merged.at[sample_id, 'diff_frame_sample'] = frame_time_smi - sample_time
        df_merged.at[sample_id, 'frame_n'] = frame_n
        df_merged.at[sample_id, 'frame_id'] = frame_id

    df_merged = df_merged.dropna(subset = ['frame_id'])
    df_merged = df_merged.reset_index()

    m_time_dif = round(df_merged.diff_frame_sample.mean(), 2)
    max_time_dif = int(df_merged.diff_frame_sample.max())
    min_time_dif = int(df_merged.diff_frame_sample.min())

    print(f"LOG\tAverage time difference between sample and frame is m = {m_time_dif} ms, range: {max_time_dif} ms - {min_time_dif} ms")

    return df_merged


def takeClosest(time_list, time):

    """
    Works only on renamed frames.
    from: https://stackoverflow.com/a/12141511/7285920
    Assumes time_list is sorted. Returns closest value to time.

    If two numbers are equally close, return the smallest number.
    """

    pos = bisect_left(time_list, time)
    if pos == 0:
        return time_list[0], 0
    if pos == len(time_list):
        return time_list[-1], len(time_list)

    pos_before = pos - 1
    pos_after = pos

    before = time_list[pos_before]
    after = time_list[pos_after]
    if after - time < time - before:
        return after, pos_after
    else:
        return before, pos_before


def tidy(df):

    tidy_df = df[
        ['subject_location', 'subject_id', 'date', 'daytime',
        'setting_id', 'trial_id',
        'sample_id', 'sample_n', 'sample_rate', 'sample_time_smi',
        'frame_id', 'frame_n', 'frame_rate', 'frame_time_smi',
        'fix_pos_x', 'fix_pos_y']
        ]

    return tidy_df


def run(subject_id, draw = True, export_duration = 30):


    subject_id = str(subject_id).zfill(2)
    print(f"<<< START SETTING FIRST FRAMES FOR subject_{subject_id} >>>")

    dir_rl = Path('03-data', '01-preprocessing', 'preprocessed_data', f'subject_{subject_id}', 'rl')
    file_onsets = dir_rl.joinpath(f'onsets_{subject_id}.txt')
    dir_log = dir_rl.joinpath('logs')
    dir_log.mkdir(exist_ok = True)

    if not file_onsets.is_file():
        print(f'ERROR\tDefine onsets first for {subject_id}')

    else:

        df_onsets = pd.read_csv(file_onsets, sep='\t')

        for trial_id in df_onsets.trial_id.unique():

            file_smilog = dir_rl.parent.parent.parent.joinpath(
                'exports_rl', 'metrics_export',
                f'Raw Data - Raw Data - {str(int(subject_id))}_{trial_id}.txt')

            if not file_smilog.is_file():
                print(f'ERROR\tNo SMI metrics export for {subject_id} at {trial_id}')
            else:
                print(f'LOG\tFound SMI metrics export for {subject_id} at {trial_id}')
                df_log = extractLog.run(file_smilog)

                first_frame_n = int(df_onsets.loc[df_onsets['trial_id'] == trial_id].first_frame_n)
                first_frame_time = int(df_onsets.loc[df_onsets['trial_id'] == trial_id].first_frame_time)
                frame_rate = df_log.frame_rate[0]
                frames_dir = dir_rl.joinpath('frames_tmp', trial_id)

                # and all frames in that directory
                list_frames = list(frames_dir.glob('*.png'))

                drawed_fixations = frames_dir.parent.parent.joinpath('frames', trial_id).is_dir()
                if not list_frames and not drawed_fixations:
                    print(f'ERROR\tNo previously exported frames and no preprocessed onsets for {subject_id}, {trial_id}')

                else:

                    # if all frames should be exported (from the first set frame on or for validation videos)
                    if trial_id.startswith('val'):
                        trial_duration = (len(list_frames)-first_frame_n)/frame_rate
                    else:
                        trial_duration = export_duration

                    df_frames = assignOnsets(list_frames, frame_rate, first_frame_time, first_frame_n, trial_duration)
                    df_merged = mergeLogs(df_frames, df_log)
                    df_merged = tidy(df_merged)

                    file_merged = dir_log.joinpath(f'log_{subject_id}_{trial_id}.txt')
                    df_merged.to_csv(file_merged, sep='\t')

        if draw:
            drawFixations.run(dir_rl)
