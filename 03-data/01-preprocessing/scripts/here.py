#!/usr/bin/python
# coding: utf-8

# by Jonas Großekathöfer, 2019

"""
TODO
"""

import os

def here(file_name='.here'):

    """
    from: https://stackoverflow.com/questions/37427683/python-search-for-a-file-in-current-directory-and-all-its-parents
    adapted to work with here-package in R and set a proper working directory from project root
    """

    current_dir = os.getcwd()  # search starts here
    # print(current_dir)

    while True:
        file_list = os.listdir(current_dir)
        parent_dir = os.path.dirname(current_dir)
        if file_name in file_list:
            # print('File Exists in: ', current_dir)
            break

        else:
            if current_dir == parent_dir:  # if dir is root dir
                # print('File not found')
                break
            else:
                current_dir = parent_dir

    return current_dir
