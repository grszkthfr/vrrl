#!/usr/bin/python
# coding: utf-8

# by Jonas Großekathöfer, 2019

"""

This script contains the workflow to extract all frames from videos recorded by
the SMI eye tracker in the HTC vive and extracts the per subject log files from
the genreal SMI log exported with BeGaze.

"""

import os
from os import path # TODO replace with pathlib
# import subprocess
import glob
import numpy as np
import pandas as pd
import cv2 as cv2
import here


def makeDirs(subject_id, trial_id):

    output_frames_dir = path.join(
        here.here(),
        '03-data', '01-preprocessing', 'preprocessed_data',
        'subject_' + subject_id, 'vr', 'frames_tmp', trial_id)

    input_frames_dir = path.join(
        here.here(),
        '02-experiment', 'vr', 'raw_data',
        'subject_' + subject_id, 'frames', trial_id)

    output_logs_dir = path.join(
        here.here(),
        '03-data', '01-preprocessing', 'preprocessed_data',
        'subject_' + subject_id, 'vr', 'logs')

    os.makedirs(output_frames_dir, exist_ok=True)
    os.makedirs(output_logs_dir, exist_ok=True)

    return input_frames_dir, output_frames_dir, output_logs_dir


def updateLog(log_file, output_logs_dir):

    log = pd.read_csv(log_file, sep='\t')
    setting_id = log.setting_id[0]

    log['tmp_frame_id'] = log.frame_id.str[:-4]
    log.tmp_frame_id = pd.to_numeric(log.tmp_frame_id)
    log.frame_rate = pd.to_numeric(log.frame_rate)
    log.subject_id = log.subject_id.map(lambda x: str(x).zfill(2))

    # remove unscreenshoted frame_id
    log.loc[
        # frames off the frame_rate
        (log.tmp_frame_id % log.frame_rate != 0) |
        # frames before first
        (log.tmp_frame_id <= 250) |
        #frames after last -> empty
        (log.tmp_frame_id > 1000), 'frame_id'] = ''


    # rename the rest
    log.loc[
        (log.tmp_frame_id % log.frame_rate == 0) &
        (log.tmp_frame_id > 250) &
        (log.tmp_frame_id <= 1000), 'frame_id'] = log.frame_id.str[:-4] + '.png'

    # drop tmp column
    log = log.drop('tmp_frame_id', axis=1)

    # adapted fix_pos_* to downscaled video, and flip y coord
    log.fix_pos_x = log.fix_pos_x.map(lambda x: int(x)/2)
    log.fix_pos_y = log.fix_pos_y.map(lambda x: 840-(int(x)/2))

    # coordinates for exactly each frame_id (in vr there are sometimes two samples
    # assigned to one frame, fixing by averaging the coordinates, averaging has
    # no effect, if only one value per frame_id)
    log_fix_pos_m = log.drop(log[log.frame_id==''].index).groupby(['frame_id'])['fix_pos_x', 'fix_pos_y'].mean()

    # merge means with original log
    log = pd.merge(log, log_fix_pos_m, on='frame_id', suffixes = ('', '_m'), how='left')

    # make indicator column if coords had been averaged
    log['fix_pos_avrgd'] = np.where((log.fix_pos_x == log.fix_pos_x_m) &
                                    (log.fix_pos_y == log.fix_pos_y_m),
                                    0, 1)
    # code only for samples with frame_id
    log.fix_pos_avrgd = np.where(log.frame_id == '', '', log.fix_pos_avrgd)

    log['fix_pos_diff_x'] = log.fix_pos_x - log.fix_pos_x_m
    log['fix_pos_diff_y'] = log.fix_pos_y - log.fix_pos_y_m

    # print(log_selection[['subject_id', 'frame_id', 'trial_id', 'fix_pos_avrgd', 'fix_pos_diff_x', 'fix_pos_diff_y' ]].drop(log_selection[log_selection.fix_pos_avrgd != 1].index))

    # rename first fix_pos_* into fix_pos_*_old, and take the averaged fix_pos_* as default.
    log.rename(columns={'fix_pos_x': 'fix_pos_x_old', 'fix_pos_y':'fix_pos_y_old'}, inplace = True)
    log.rename(columns={'fix_pos_x_m': 'fix_pos_x', 'fix_pos_y_m': 'fix_pos_y'}, inplace=True)

    log.replace('', np.nan, inplace = True)
    log.dropna(subset = ['frame_id'], inplace = True)
    log.reset_index(drop=True, inplace=True)

    output_file = path.join(output_logs_dir, f'log_{path.basename(log_file)}')

    log.to_csv(
            output_file,
            sep='\t')

    return setting_id


def convertFrames(input_dir, output_dir):

    for frame in glob.glob(path.join(input_dir, '*.ppm')):

        frame_id = path.basename(frame)[:-4]

        frame_in = cv2.imread(frame)
        frame_in = cv2.flip(frame_in, 0)
        height, width, channels = frame_in.shape

        cv2.putText(
            frame_in,
            frame_id, (width-100, height-800),
            cv2.FONT_HERSHEY_PLAIN, 2, (0, 255, 255))

        frame_out = cv2.imwrite(
            path.join(output_dir, frame_id + '.png'),
            frame_in)


# def mergeProt(log):

#     subject_id = str(log.subject_id[0]).zfill(2)
#     prot_file = path.join(
#         '02-experiment', 'vr', 'prot',
#         'prot_' + subject_id + '.csv')
#     prot = pd.read_csv(prot_file, sep=';', decimal=',')

#     merged_df = pd.merge(
#         log,
#         prot,
#         on=('subject_id', 'setting_id', 'trial_id'),
#         how='inner')

#     merged_df.subject_id = subject_id

#     return merged_df


def run(data_dir, convert_frames=True, update_log=True, overwrite_frames=False):

    """

    TODO

    """

    print("<<< START CONVERTING .ppm TO .png >>>")

    # comment!
    for log_file in glob.glob(data_dir):

        subject_id = path.basename(log_file)[0:2]
        trial_id = path.basename(log_file)[3:-4]

        # rd = raw_data, ed = exported_data
        rd_frames_dir, ed_frames_dir, ed_logs_dir = makeDirs(subject_id, trial_id)

        if len(os.listdir(ed_frames_dir)) == 0:
            if convert_frames: # TODO skip, if exist: overwrite_frames
                convertFrames(rd_frames_dir, ed_frames_dir)
                print(f"LOG:\tConverted .ppm to .png for subject {subject_id} at location {trial_id}!")
                
            if update_log:
                setting_id = updateLog(log_file, ed_logs_dir)
                print(f"LOG:\tUpdated log for subject {subject_id} at location {trial_id}!")

        # if dir is not empty:
        else: 
            print(f"LOG:\tSkipped preprocessing for {subject_id} at location {trial_id}. Export direcotry not empty!")
