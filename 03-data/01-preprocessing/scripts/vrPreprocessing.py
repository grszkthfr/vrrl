#!/usr/bin/python
# coding: utf-8

# by Jonas Großekathöfer, 2019

"""
TODO
"""

import os.path as path  # TODO replace with pathlib
import convertFrames
import drawFixations
import here

rd_dir = path.join(
    here.here(),
    '02-experiment',
    'vr',
    'raw_data',
    'subject_*',
    'logs',
    '*.txt')

pd_dir = path.join(
    here.here(),
    '03-data',
    '01-preprocessing',
    'preprocessed_data',
    'subject_*',
    'vr')


# convert .ppm to .png
convertFrames.run(rd_dir)  # in case of multiple sample per frames, it
                           # calculates the average fixation coordinates.

# draw fixations in preprocessd frames
drawFixations.run(pd_dir)

print("<<< PREPROCESSING DONE! >>")
