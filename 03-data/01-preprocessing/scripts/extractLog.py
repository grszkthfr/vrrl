#!/usr/bin/python
# coding: utf-8

# by Jonas Großekathöfer, 2020

"""This script contains the workflow to extract all exported metrics (exported
by Begaze) by the SMI mobile eye tracker and returns per subject log files from
the genreal SMI log exported with BeGaze.

"""

import os
from pathlib import Path
# import subprocess
import pandas as pd
# import numpy as np
import here

def splitSmiLog(log_path):

    """
    This function splits the complete log exported from BeGaze and returns to
    separate data frames containing BeGaze information and eyetracking data.
    """

    file_path = log_path

    if not file_path.exists():
        error_msg = f"ERROR\tNo SMI log at: {file_path}"
        # log_error(input_video, error_msg)
        print(error_msg)
        success, log_begaze, log_data = False, [], []

        return success, log_begaze, log_data

    # Magic I don't understand: Start!
    # from https://stackoverflow.com/a/34188535/7285920
    df = pd.read_csv(
        file_path,
        header=None,
        names=range(41),  # export has a max of 41 columns
        sep='\t',
        dtype=str)  # all columns (which should not exist here :-/) to str

    table_names = ["## [BeGaze]", "## [Data]"]  # SMI separators for two data
                                                # within a single file

    groups = df[0].isin(table_names).cumsum()
    tables = {g.iloc[0,0]: g.iloc[1:] for table_name, g in df.groupby(groups)}

    for table_name, v in tables.items():
        v = v.rename(columns=v.iloc[0])
        v = v.drop(v.index[0])
        file_id = table_name[4:-1].lower()
    # Magic I don't understand: Stop!

        success = True
        # Write BeGaze information to data frame
        if file_id == "begaze":
            log_begaze = v.dropna(axis=1)
            # print(log_begaze.(5))

        # Write eyetracking inforamtion to data frame
        elif file_id == "data":
            log_data = v.dropna(axis=1)
            # print(log_data.(5))

        else:
            success = False
            print("ERROR\tProblems splitting complete log file!")

    return success, log_begaze, log_data


def getMillisecondsFromString(time_str):

    """
    Simple conversion of the time string provided in the SMI log to integer in
    milliseconds
    """

    # Split string into hours, minutes, seconds, milliseconds
    h, m, s, ms = time_str.split(':')
    return int(h) * 3600*1000 + int(m) * 60*1000 + int(s) * 1000 + int(ms)


def tidyLog(log, sample_rate, date):

    """
    This script contains the workflow to extract specific frames from the
    videos recorded by the smi mobile eyetracker and give then an unambiguously
    file name.
    """

    # Read data
    log_et = log.reset_index(drop=True)
    sample_ms = int(1000/int(sample_rate)) # sample happens every sample_ms

    # Prepare data
    # Rename relevant columns
    log_et = log_et.rename(
        columns={
            'Participant': 'subject_location',
            'Time of Day [h:m:s:ms]': 'daytime',
            'Video Time [h:m:s:ms]': 'video_time_str',
            'Stimulus': 'video_raw_id',  # actually not used for finding video
            'RecordingTime [ms]': 'sample_time_smi',
            'Point of Regard Binocular X [px]': 'fix_pos_x',
            'Point of Regard Binocular Y [px]': 'fix_pos_y'})

    # Convert to int, necessary unfortunately from split complete data frame
    log_et['sample_time_smi'] = pd.to_numeric(log_et['sample_time_smi'], errors='coerce')
    log_et['fix_pos_x'] = pd.to_numeric(log_et['fix_pos_x'], errors='coerce')
    log_et['fix_pos_y'] = pd.to_numeric(log_et['fix_pos_y'], errors='coerce')

    log_et['sample_time_smi'] = log_et['sample_time_smi'].fillna(0)
    log_et['fix_pos_x'] = log_et['fix_pos_x'].fillna(0)
    log_et['fix_pos_y'] = log_et['fix_pos_y'].fillna(0)

    log_et.sample_time_smi = log_et.sample_time_smi.astype(int)
    log_et.fix_pos_x = log_et.fix_pos_x.astype(int)
    log_et.fix_pos_y = log_et.fix_pos_y.astype(int)


    # Get some important values
    sample_start = log_et.sample_time_smi[0]
    sample_end = log_et.sample_time_smi[len(log_et)-1]
    sample_duartion = sample_end - sample_start
    n_samples = len(log_et.index)

    # Calculate new columns
    log_et['sample_time_ms'] = log_et.sample_time_smi - sample_start + sample_ms
    log_et['sample_rate'] = int(sample_rate)

    # Convert time string to milliseconds
    log_et['video_time_ms'] = log_et.video_time_str.apply(getMillisecondsFromString)

    # Count samples
    log_et['sample_id'] = range(1, n_samples+1)

    # set setting
    log_et['setting_id'] = 'rl'

    # tidy subject and location columns
    log_et['subject_id'], log_et['trial_id'] = log_et.subject_location.str.split('_', 1).str

    # fix in case subject_location/subject_id has no 0 when < 9
    log_et.subject_id = log_et.subject_id[0].zfill(2)
    log_et.subject_location = log_et.subject_location[0].split('_')[0].zfill(2) +'_' + log_et.subject_location[0].split('_')[1]

    # date
    log_et['date'] = date.split()[0].replace('.', '-')  # some formating

    # frame rate
    log_et['frame_rate'] = 25

    log_et = log_et[[
        'subject_location', 'subject_id', 'date', 'daytime',
        'setting_id', 'trial_id',
        'sample_id', 'sample_rate', 'sample_time_smi', 
        # 'sample_time_ms', 'video_time_str', 'video_time_ms',
        'frame_rate',
        'fix_pos_x', 'fix_pos_y']]

    return log_et

def makeOutputDir(subject_id, trial_id):

    """
    This function takes the subject_location from the log file and creates
    directories for frames and log.
    """

    # print(subject_id, trial_id)

    # for log files
    output_log = Path(
        '03-data', '01-preprocessing', 'preprocessed_data',
        'subject_' + subject_id, 'rl','logs')
    os.makedirs(output_log, exist_ok=True)

    return output_log


def run(log_file):

    """
    For each subject_location exported from BeGaze save all frames from video
    and save associated eyetracking log file
    """

    # Get BeGaze and eye tracking data
    success, log_begaze, log_data = splitSmiLog(log_file)

    if success:
        n_records = len(log_begaze.index)
        print(f"LOG\tFound {n_records} records.")

        # Take each subject_location and it's sample rate in log_begaze, and...
        for subject_location, sample_rate, date in zip(
                log_begaze['Participant'],
                log_begaze['Sample Rate'],
                log_begaze['Recorded On']):

            subject_id = subject_location.split("_")[0].zfill(2)

            trial_id = subject_location.split("_")[1]

            pd_logs_dir = makeOutputDir(subject_id, trial_id)

            # Select subject_location specific rows from complete data frame
            log_samples = log_data[log_data['Participant'] == subject_location]

            # Clean log
            log_samples = tidyLog(
                    log = log_samples,
                    sample_rate = sample_rate,
                    date = date)

            # file_samples = pd_logs_dir.joinpath(f'logsmi_{subject_id}_{trial_id}.txt')
            # log_samples.to_csv(file_samples)

            return(log_samples)
