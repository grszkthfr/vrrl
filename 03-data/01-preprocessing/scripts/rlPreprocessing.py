#!/usr/bin/python
# coding: utf-8

# by Jonas Großekathöfer, 2019

"""
TODO
"""

import setFrames

subject_ids = list(range(1,44+1))

for subject_id in subject_ids:
    setFrames.run(subject_id, draw=True)

print("<<< PREPROCESSING DONE! >>>")
