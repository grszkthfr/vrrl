#!/usr/bin/python
# coding: utf-8

# by Jonas Großekathöfer, 2019

"""

This script adds extracted frames to the log-file with SMI data by letting
the user choose the first frame, enter the SMI timestamp.

"""

from pathlib import Path
import pandas as pd
# import subprocess
import glob
import cv2 as cv2

import here

# inventing a gui
from PyQt5.QtWidgets import QApplication, QWidget, QLabel, QLineEdit, QInputDialog  # , QVBoxLayout
# from PyQt5.QtGui import QIcon, QPixmap, QImage


def selectFirstFrame(frames):

    i = 0
    while i <= len(frames):
        print(frames[i])
        cv2.namedWindow("Select first frame")
        img = cv2.imread(str(frames[i]))

        find_first_frame = True
        while find_first_frame:

            cv2.imshow('Select first frame', img)
            cv2.putText(
                img,
                text=str(frames[i]),
                fontFace=cv2.FONT_HERSHEY_PLAIN,
                org=(10, 50),
                fontScale=1.2,
                color=(0, 0, 255),
                lineType=2)

            k = cv2.waitKey(33)
            if k == 110:  # (n)ext frame
                i += 1
                img = cv2.imread(str(frames[i]))

            elif k == 112:  # (p)revious frame
                i -= 1
                img = cv2.imread(str(frames[i]))

            elif k == 115:  # (s)et frame as >first frame<

                first_frame_n = i
                # first_frame_smi_timestamp = input(
                #     'Please enter SMI timestamp: ')
                find_first_frame = False
                i = len(frames) + 1  # to stop the while loop :-/

                cv2.imshow('Select first frame', scaleIamge(img))
                first_frame_smi_timestamp = inputSmiTime()
                print(f"LOG\tSMI timestamp for first frame is {first_frame_smi_timestamp}")

                break

    cv2.destroyAllWindows()

    return first_frame_n, first_frame_smi_timestamp


def scaleIamge(image):

    image = image[0:75, 0:125]
    scale_percent = 250 # percent of original size
    width = int(image.shape[1] * scale_percent / 100)
    height = int(image.shape[0] * scale_percent / 100)
    # resize image
    image = cv2.resize(image, (width, height), interpolation = cv2.INTER_AREA)
                #   R  B   G
    # boundaries = [
    #     ([12, 10, 90], [75, 75, 225]),
    #     ([245, 0, 245], [255, 15, 255])
    #     ]
    # for (lower, upper) in boundaries:
    #     # create NumPy arrays from the boundaries
    #     lower = np.array(lower, dtype = "uint8")
    #     upper = np.array(upper, dtype = "uint8")

    #     # find the colors within the specified boundaries and apply
    #     # the mask
    #     mask = cv2.inRange(image, lower, upper)
    #     output = cv2.bitwise_and(image, image, mask = mask)

    # # show the images
    # cv2.imshow("images", image)
    # cv2.waitKey(0)

    # app = QApplication([])
    # input_field = QLineEdit('')
    # # timestamp, ok = QInputDialog.getInt(input_field, "Trial start time", "Enter SMI timestamp")
    # label = QLabel()
    # qimage = QImage(image, image.shape[0],image.shape[1],QImage.Format_RGB16)
    # pixmap = QPixmap(qimage)
    # label.setPixmap(pixmap)
    # label.show()
    # app.exec_()

    return image


def inputSmiTime():

    app = QApplication([])
    input_field = QLineEdit('')
    timestamp, ok = QInputDialog.getInt(input_field, "Trial start time", "Enter SMI timestamp")

    # # Create widget
    # label = QLabel()
    # pixmap = QImage(frame)
    # label.setPixmap(pixmap)
    # pixmap.resize(pixmap.width(),pixmap.height())
    # pixmap.show()
    # app.exec_()

    return timestamp


def run(data_dir):

    print("<<< START SETTING FIRST FRAMES >>>")

    # find all logs and save in list
    for subject_dir in data_dir.glob('subject_*'):

        subject_id = subject_dir.name.split("_")[1]

        # prepare onset df
        file_onsets = data_dir.joinpath(
            subject_dir, 'rl',
            'onsets_' + subject_id + '.txt')
        cols_onsets = ['subject_id', 'setting_id', 'trial_id', 'first_frame_n', 'first_frame_time', 'first_frame_id']
        list_onsets = []

        print(f'LOG\tCode onsets for subject_{subject_id}.')

        if not file_onsets.is_file():
            df_onsets = pd.DataFrame(list_onsets, columns = cols_onsets)
            df_onsets.to_csv(file_onsets, sep='\t', index=False)

        # read prev coded onsets
        df_onsets = pd.read_csv(file_onsets, sep='\t')
        
        frames_dirs = data_dir.joinpath(
            'subject_' + subject_id,
            'rl',
            'frames_tmp').glob('*')
        
        # all (remaining) 'all_frames' dirs
        for frames_dir in list(frames_dirs):

            trial_id = str(frames_dir.name)
            if not trial_id in df_onsets.trial_id.unique():

                print(f"LOG:\tFound missing onsets for trial_{trial_id}")

                # and all frames in that directory
                frames = list(frames_dir.glob('*.png'))

                # test if frames were found
                if frames:
                    first_frame_n, first_frame_time = selectFirstFrame(frames)
                    first_frame_id = frames[first_frame_n].name
                    
                    onsets_add = [int(subject_id), 'rl', str(trial_id), str(first_frame_n), str(first_frame_time), str(first_frame_id)]
                    df_onsets_add = pd.DataFrame([onsets_add], columns = cols_onsets)
                    df_onsets = df_onsets.append(df_onsets_add)
  
                else:
                    print(f"LOG:\tEmpty directory: {frames_dir}!")
    
            else:
                print(f'LOG\tOnsets exist for {trial_id}')
        
        df_onsets.to_csv(file_onsets, sep='\t', index=False)



# preprocessed_data
pd_dir = Path(
    here.here(),
    '03-data',
    '01-preprocessing',
    'preprocessed_data')

# # prepare for drawing fixations
run(pd_dir)  # frames need to be already
                                # extracted
