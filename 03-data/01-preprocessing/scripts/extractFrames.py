#!/usr/bin/python
# coding: utf-8

# by Jonas Großekathöfer, 2019

"""
This script contains the workflow to extract all frames from videos
recorded (and exported by Begaze) by the SMI mobile eye tracker, if preivously no onsets were extracted.
"""

from pathlib import Path
# import subprocess
import pandas as pd
import cv2


# https://stackoverflow.com/a/34325723/7285920 never checked code
def printProgressBar(iteration, total, prefix='', suffix='', decimals=1,
                      length=100, fill='█'):
    """
    Call in a loop to create terminal progress bar
    @params:
        iteration   - Required  : current iteration (Int)
        total       - Required  : total iterations (Int)
        prefix      - Optional  : prefix string (Str)
        suffix      - Optional  : suffix string (Str)
        decimals    - Optional  : positive number of decimals in percent complete (Int)
        length      - Optional  : character length of bar (Int)
        fill        - Optional  : bar fill character (Str)
    """
    percent = ("{0:." + str(decimals) + "f}").format(100 * (iteration /
                                                            float(total)))
    filledLength = int(length * iteration // total)
    bar = fill * filledLength + '-' * (length - filledLength)
    print('\r%s |%s| %s%% %s' % (prefix, bar, percent, suffix), end = '\r')
    # Print New Line on Complete
    if iteration == total:
        print()


def writeFrames(input_video, output_dir, export_frames=True):

    """
    TODO more comments!
    This script contains the workflow to extract specific
    frames from the videos recorded by the smi mobile eyetracker and give then
    an unambiguously file name.

    """

    #################### ffmpeg extract ######################################

    # print("Start extrtacting frames with ffmpeg!\n...")
    # print(input_video)
    # print(output_dir)
    # # https://stackoverflow.com/questions/2017843/fetch-frame-count-with-ffmpeg
    # # only valid method:
    # # ffprobe = subprocess.Popen(
    # # "ffprobe -v error -count_frames -select_streams v:0 -show_entries stream=nb_read_frames -of default=nokey=1:noprint_wrappers=1 {}". format(file))

    # ffmpeg = subprocess.Popen(
    #     "ffmpeg -i {} -r 1/1 {}"
    #     .format(str(input_video), str(output_dir.joinpath('%05d.png'))))

    # ffmpeg.wait()

    #################### cv2 extract ########################################
    cap = cv2.VideoCapture(str(input_video))

    # only for progressbar, not accurate!
    max_frames = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))/40  # query container
                                                            # gives wrong
                                                            # values: one frame
                                                            # (file) more than
                                                            # supposed to, so
                                                            # it has to be
                                                            # counted manually
                                                            # after extraction

    # frame_rate = int(cap.get(cv2.CAP_PROP_FPS))  # not working with new decoder
    #                                            selected in BeGaze
    frame_rate = 25  # setting from BeGaze

    output_dir.mkdir(parents = True, exist_ok = True)

    success, frame = cap.read()  # read first frame
    frame_n = 0
    success = True

    if export_frames:
        print("LOG\tStart extrtacting frames with cv2!")

    while success:

        frame_file = Path(output_dir).joinpath(f"{str(frame_n).zfill(5)}.png")

        if export_frames:
            cv2.imwrite(
                str(frame_file),
                frame)
        printProgressBar(frame_n, max_frames)

        success, frame = cap.read()  # read next frame

        # print(count, frame)
        # print ('Read a new frame: ', success)
        frame_n += 1

    ####################################################################

    # nth frame is last/max frame
    n_frames = frame_n

    # All frames in a list, sorted by file name
    frames = sorted(list(frame_file.parent.glob("*.png")))
    print(frame_file.parent)
    # Check number of files has to equal n_frames, otherwise something went
    # wrong
    if len(frames) == n_frames:
        success = True

    else:
        print(f"ERROR\tExtracted {len(frames)} frames, but counted {n_frames}.")

    success = True

    return success, frame_rate


def run(video_export_dir):

    video_list = list(video_export_dir.glob('*.avi'))
    print(f"LOG\tFound {len(video_list)} records.")

    for video_file in video_list:

        subject_id, trial_id = str(video_file.name).split("_")
        subject_id = subject_id.zfill(2)
        trial_id = trial_id.split(".")[0]

        preprocessed_dir = video_export_dir.parent.parent.joinpath('preprocessed_data', f'subject_{subject_id}', 'rl')

        file_onsets = preprocessed_dir.joinpath(f'onsets_{subject_id}.txt')

        if file_onsets.is_file():

            onsets = pd.read_csv(file_onsets, sep='\t')

            if trial_id in onsets.trial_id.unique():
                    print(f'LOG\tFound onsets for {subject_id} at {trial_id}, assume completed preprocessing')

            else:
                frames_dir = preprocessed_dir.joinpath('frames_tmp', trial_id)

                if not frames_dir.is_dir():
                    print(f'LOG\tStart extracting frames for {subject_id} at {trial_id}')
                    success, _ = writeFrames(video_file, frames_dir)

                else:
                    print(f'LOG\tDirectory frames_tmp not empty for {subject_id} at {trial_id}, assume extracted frames')

        else:
            print(f'LOG\tStart extracting frames for {subject_id} at {trial_id}')
            frames_dir = preprocessed_dir.joinpath('frames_tmp', trial_id)
            print(frames_dir)
            success, _ = writeFrames(video_file, frames_dir)


run(Path('03-data', '01-preprocessing', 'exports_rl', 'video_exports'))
