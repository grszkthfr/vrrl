# Preprocessing

## Content

* data directories:
    * exports_rl
        * *metrics_exports*:
            * exported (processed by SMI BeGaze) metrics for each participant
              at each location.
        * *video_exports*:
            * exported (processed by SMI BeGaze) videos for each participant
              at each location.
    * preprocessed data
        * for each subject subfolders for *vr* and *rl*
        * subfolders:
            * frames_tmp: for each location a folder with **all** frames from
              exported videos, **without** fixation circle
            * logs: for each location a text file with all relevant samples for
              relevant frames
            * frames: fore each location a folder with the final (to be coded)
              frames with drawn fixations (from logs), for trials (vs.
              validation) there should be 150 frames per location (75 per
              folder in *vr*)
* *scripts*

## Fixation indicator (i.e. the magenta circle)

* Field-of-View (FOV) of the SMI-Camera: 60° horizontal, 46° vertical; 1280p;
  1280x960p: Fixation
  * 22p = 60°, 1p = 0.046875°, 22p = 1,03125°
  * Default setting of SMI Software
* FOV of the Vive:
  [about](https://www.vrheads.com/field-view-faceoff-rift-vs-vive-vs-gear-vr-vs-psvr)
  100° horizontal, 110° vertical (distance to lense: 10 mm); 1512x1680 /internal
  rendering resolution/
  * 1512p = 100°, 15.12p = 1°

## Workflow

### RL

1. Videos and metrics exported with BeGaze (for instructions see
   [here](02-experiment/rl/README.md))
2. `extractFrames.py` to get frames per participant and location
3. `setOnset.py` to set onsets of trials
4. `rlPreprocessing.py`
   1. `setFrames.py` to find frames starting from onset
   2. `rlExtractLog.py` to get per participant per location logs
   3. `drawFixations.py`

### VR

1. `vrPreprocessing.py` to convert from .ppm to .png

### Remove *raw* frames

To save some disk space remove *frames_tmp* with something like:
`rm -rf 03-data/01-preprocessing/preprocessed_data/subject_*/**/frames_tmp `
