## 03-data ##

### 01-preprocessing ###

Prepares the eye tracking data (exported from the *vr* and *rl* setting) for
coding fixations with the fixation coder.

### 02-postprocessing ###

Prepares the data to be analyzed. Contains the final data sets.

### 03-quality_checks ###

For a potential drift corrections find the distance of measured and actual
fixation point from validation trials and calculate distance.
