#!/usr/bin/env bash

# further imporvements: paths handling

# number of subjects, including a leading 0
for subject in {01..44};
do
    echo "subject: $subject"
    # each setting
    for setting in rl vr;
    do
        # each location
        for location in bac gas loc rat reg valpre valpost;
        do
            echo "location: $location"
            # in rl
            if [ $setting = "rl" ]
            then
                echo "rl: $setting"
                frames=03-data/01-preprocessing/preprocessed_data/subject_"$subject"/"$setting"/frames/"$location"/*.png
                video=06-miscellaneous/videos/"$setting"_"$subject"_"$location".mkv
                echo "here are the frames: $frames"
                echo "this will be the video name: $video"
                ffmpeg -framerate 5 -pattern_type glob -i "$frames" "$video"
            # in vr
            else
                echo "vr: $setting"
                # trials exists only for not-validation locations
                if [[ $location != val* ]]
                then
                    # for each half location, i.e. trial
                    for trial in {1..2};
                    do
                        echo "trial: $trial"
                        frames=03-data/01-preprocessing/preprocessed_data/subject_"$subject"/"$setting"/frames/"$location"_"$trial"/*.png
                        video=06-miscellaneous/videos/trials/"$setting"_"$subject"_"$location"_"$trial".mkv
                        text=06-miscellaneous/videos/trials/"$setting"_"$subject"_"$location".txt
                        echo "here are the frames: $frames"
                        echo "this will be the video name: $video"
                        ffmpeg -framerate 5 -pattern_type glob -i "$frames" "$video"
                        echo "file ./"$setting"_"$subject"_"$location"_"$trial".mkv" >> $text # relative to txt file
                    done
                    # one video for location
                    video=06-miscellaneous/videos/"$setting"_"$subject"_"$location".mkv
                    echo "later another $video"
                    ffmpeg -f concat -safe 0 -i "$text" -c copy "$video"
                # validation trials in vr
                else
                    echo "rl: $setting"
                    frames=03-data/01-preprocessing/preprocessed_data/subject_"$subject"/"$setting"/frames/"$location"/*.png
                    video=06-miscellaneous/videos/"$setting"_"$subject"_"$location".mkv
                    echo "here are the frames: $frames"
                    echo "this will be the video name: $video"
                    ffmpeg -framerate 5 -pattern_type glob -i "$frames" "$video"
                fi
            fi
        done
    done
done
