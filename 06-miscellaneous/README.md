
# Table of Contents

1.  [videos](#videos)
    1.  [crop rl videos](#crop-rl-videos)


<a id="videos"></a>

# videos

Run `./06-miscellaneous/frames-to-video.sh` from project root.


<a id="crop-rl-videos"></a>

## crop rl videos

1.  crop rl videos to vr dimensions with: `ffmpeg -i rl_35_bac.mkv -filter:v
       "crop=754:840" croped_rl_35_bac.mkv`
2.  (optional) put them next to each other with (found [here](https://unix.stackexchange.com/a/233833)):

    ffmpeg \ -i croped_rl_35_bac.mkv \ -i vr_35_bac.mkv \ -filter_complex
    '[0:v]pad=iw*2:ih[int];[int][1:v]overlay=W/2:0[vid]' \ -map "[vid]" \ -c:v
    libx264 \ -crf 23 \ -preset veryfast \ output.mkv

