
# Table of Contents

1.  [Einverständiserklärung](#einverständiserklärung)
2.  [DSGVO](#dsgvo)
3.  [Fragebögen](#fragebögen)

Contains the participants and GDPR agreement as well as copies of
questionnaires.


<a id="einverständiserklärung"></a>

# Einverständiserklärung

-   *Komplett*: Gamer<sub>ERC</sub><sub>WPC</sub><sub>Experiment3.docx</sub>
-   *Komplett (mit Anpassung)*: Gamer<sub>ERC</sub><sub>WPC</sub><sub>Experiment3</sub><sub>jg.docx</sub>
-   *Einzelteile*:
    -   *Allgemeine Info*: pro-erhebung/eve<sub>info.pdf</sub>
    -   *Aufklärung*: pro-erhebung/eve<sub>aufklärung.pdf</sub>
    -   *Unterschrift*: pro-vp/eve<sub>vp.pdf</sub>


<a id="dsgvo"></a>

# DSGVO

-   [Ethikkommission des Institut für Psychologie](https://www.psychologie.uni-wuerzburg.de/forschung/ethikkommission/)
    -   [Download v1.01](https://www.psychologie.uni-wuerzburg.de/fileadmin/06020000/download/DSGVO_Psychologie_v1.01.docx)


<a id="fragebögen"></a>

# Fragebögen

-   [AQ-K, deutsch](https://econtent.hogrefe.com/doi/full/10.1026/1616-3443.36.4.280)
    -   [original](https://link.springer.com/article/10.1023%2FA%3A1005653411471)
-   [SPAI](https://www.sciencedirect.com/science/article/pii/S0005789489800607)
-   [SSQ](https://www.tandfonline.com/doi/abs/10.1207/s15327108ijap0303_3)
-   [IPQ](https://www.mitpressjournals.org/doi/abs/10.1162/105474601300343603)

