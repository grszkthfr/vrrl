
# Table of Contents

1.  [make<sub>validation</sub><sub>videos</sub>](#make_validation-videos)
2.  [power<sub>estimation</sub>](#org3c40ab2)
3.  [documentation](#documentation)
    1.  [Einverständiserklärung](#einverständiserklärung)
    2.  [DSGVO](#dsgvo)
    3.  [Fragebögen](#fragebögen)

Directory for experiment preparation. Most importantly it contains the
preregistration and power estimation for the experiment. Other files are not
uploaded and only relevant for internal usage.


<a id="make_validation-videos"></a>

# make<sub>validation</sub><sub>videos</sub>

Material to create the videos for the eye tracker validation in the HMD.

-   multiply frame templates to &ldquo;frames/&rdquo; with *makeFrames.py*
    -   Length: 25 seconds, frame rate: 50fps
-   make clip with
    `ffmpeg -f image2 -r 50 -i frame_%04d.png -vcodec mpeg4 -y ../val_vid.mp4`


<a id="org3c40ab2"></a>

# power<sub>estimation</sub>

tbd


<a id="documentation"></a>

# documentation

Contains the participants and GDPR agreement as well as copies of
questionnaires.


<a id="einverständiserklärung"></a>

## Einverständiserklärung

-   *Komplett*: Gamer<sub>ERC</sub><sub>WPC</sub><sub>Experiment3.docx</sub>
-   *Komplett (mit Anpassung)*: Gamer<sub>ERC</sub><sub>WPC</sub><sub>Experiment3</sub><sub>jg.docx</sub>
-   *Einzelteile*:
    -   *Allgemeine Info*: pro-erhebung/eve<sub>info.pdf</sub>
    -   *Aufklärung*: pro-erhebung/eve<sub>aufklärung.pdf</sub>
    -   *Unterschrift*: pro-vp/eve<sub>vp.pdf</sub>


<a id="dsgvo"></a>

## DSGVO

-   [Ethikkommission des Institut für Psychologie](https://www.psychologie.uni-wuerzburg.de/forschung/ethikkommission/)
    -   [Download v1.01](https://www.psychologie.uni-wuerzburg.de/fileadmin/06020000/download/DSGVO_Psychologie_v1.01.docx)


<a id="fragebögen"></a>

## Fragebögen

-   [AQ-K, deutsch](https://econtent.hogrefe.com/doi/full/10.1026/1616-3443.36.4.280)
    -   [original](https://link.springer.com/article/10.1023%2FA%3A1005653411471)
-   [SPAI](https://www.sciencedirect.com/science/article/pii/S0005789489800607)
-   [SSQ](https://www.tandfonline.com/doi/abs/10.1207/s15327108ijap0303_3)
-   [IPQ](https://www.mitpressjournals.org/doi/abs/10.1162/105474601300343603)

