// Examples of VideoPlayer function

using UnityEngine;
using UnityEngine.Video;
using System.IO;
using SMI;
//using System.Text
using System;

// TODO do not overwrite file!
// TODO cleaning up and commenting

public class CaptureScreenshotWithGaze : MonoBehaviour
{

    //GameObject for gaze cursor
    GameObject gazeVis = null;
    Vector3 initialeScale = Vector3.zero;

    SMI.SMIEyeTrackingUnity smiInstance = null;

    //////////////////////////////////////////
    //change this line to actual subject id //
    public int subject_id = 1;  //////////////
    //////////////////////////////////////////
    //////////////////////////////////////////

    string setting_id = "vr";
    int frame_rate = 10;  // video 50FPS, every 10th -> export: 5 Frames per second
    string trial_id;
    int frame_id = 0;
    int  sample_id = 0;
    int video_frame_max = 0;
    int hmd_frame = 0;
    Vector3 screenPos;
    Vector3 camAngle;


    //Für TextWriter Eye Tracking
    TextWriter sw;

    public SMIEyeTrackingUnity smi;
    public Camera cam;

    readonly VideoClip videoClip; // old:     UnityEngine.Video.VideoClip videoClip;

    string log_header;
    string frame_log;

    static string date = DateTime.UtcNow.ToString("yyyy'-'MM'-'dd");

    private int captureWidth = 756;     // 4k = 3840 x 2160´, 1080p = 1920 x 1080, 1512 x 1680, 756 x 840
    private int captureHeight = 840;

    // optimize for many screenshots will not destroy any objects so future screenshots will be fast
    private bool optimizeForManyScreenshots = true; // necessary or make default?

    private Rect rect;
    private RenderTexture renderTexture;
    private Texture2D screenShot;

    // commands
    private bool captureScreenshot = false;

   void Awake()
   {

       //Kalibrieren();

    }

    // prepare everything: paths, names, ...
    void Start()
    {
        // write log to console
        Debug.Log("Create log file");

        //get trial_id from video player
        var videoPlayer = GameObject.Find("360sphere").GetComponent<UnityEngine.Video.VideoPlayer>();
        trial_id = videoPlayer.clip.name;
        video_frame_max = unchecked((int)videoPlayer.clip.frameCount);
        Debug.Log("trial_id is: " + trial_id + "\nmax_frame_video is: " + video_frame_max);

        smiInstance = SMIEyeTrackingUnity.Instance;

            gazeVis = (GameObject)Resources.Load("SMI_GazePoint");
            if (gazeVis != null)
            {
                gazeVis = Instantiate(gazeVis, Vector3.zero, Quaternion.identity) as GameObject;
                gazeVis.name = "SMI_GazePoint";
                initialeScale = gazeVis.transform.localScale;
            }
            else
            {
                Debug.LogError("Unity Prefab missing: SMI_GazePoint");
                UnityEditor.EditorApplication.isPlaying = false;

        }

        // prepare log
        string rd_file = "";
        // path saving log files
        string rd_path = Directory.GetCurrentDirectory() + "/raw_data" + "/subject_" + subject_id.ToString("D2") + "/logs/";
        // path saving screenshots
        string frame_path = Directory.GetCurrentDirectory() + "/raw_data" + "/subject_" + subject_id.ToString("D2") + "/frames/" + trial_id;

        // check path exists
        if (!Directory.Exists(rd_path))
            // create folder if it does not exist
            {
                Directory.CreateDirectory(rd_path);
            }

        if (!Directory.Exists(frame_path))
            // create folder if it does not exist
            {
            Directory.CreateDirectory(frame_path);
            }
        else
           {
           Debug.LogError("Directory for frames already exists! Please check subject_id or scene.");
           UnityEditor.EditorApplication.isPlaying = false;
           }

        rd_file = rd_path + subject_id.ToString("D2") + "_" + trial_id + ".txt";
        // Debug.Log(rd_file);

        // Debug.Log("Opening rd_file...");
        sw = new StreamWriter(rd_file);//, true); // append = true, don't overwrtie log file

        log_header = "setting_id" + "\t" +  "subject_id" + "\t" + "date" + "\t" + "trial_id" +
                        "\t" + "sample_id" + "\t" + "frame_rate" + "\t" + "frame_id" +
                        "\t" + "fix_pos_x" + "\t" + "fix_pos_y" +
                        "\t" + "cam_angle_x" + "\t" + "cam_angle_y" + "\t" + "cam_angle_z";
        // fehlt: frame_rate, (==) sample_rate

        // Debug.Log("Writing header...");
        sw.WriteLine(log_header);

    }

    public void CaptureScreenshot()
    {
        captureScreenshot = true; //necessary to evaluate?!
    }

    // update once per frame
    void Update()
    {
        // ? warum alles doppelt in update & in start
        var videoPlayer = GameObject.Find("360sphere").GetComponent<UnityEngine.Video.VideoPlayer>();

        // path saving log files
        string rd_path = Directory.GetCurrentDirectory() + "/raw_data" + "/subject_" + subject_id.ToString("D2") + "/logs/";
        // path saving screenshots
        string frame_path = Directory.GetCurrentDirectory() + "/raw_data" + "/subject_" + subject_id.ToString("D2") + "/frames/" + trial_id;

        if (gazeVis != null)
            UpdateThePosition();

        // count frame
        hmd_frame += 1; // (VideoPlayer.frame)
        sample_id++;
        frame_id = unchecked((int)videoPlayer.frame); // https://stackoverflow.com/questions/858904/can-i-convert-long-to-int
        //Debug.Log("frame_id: " + frame_hmd + "\nframe_video: " + frame_id);

        // coords on screen
        //"Screenspace is defined in pixels. The bottom-left of the screen is
        // (0,0); the right-top is (pixelWidth,pixelHeight). The z position is
        // in world units from the camera." (https://docs.unity3d.com/ScriptReference/Camera.ScreenToWorldPoint.html)
        screenPos = cam.WorldToScreenPoint(gazeVis.transform.position);
        camAngle = cam.transform.localEulerAngles;

        ////////////////////////////////////////////////////////////////////////

        frame_log =
            setting_id + "\t" + subject_id.ToString("D2") + "\t" + date + "\t" +
            trial_id + "\t" + sample_id.ToString("D5") + "\t" + frame_rate + "\t" + frame_id.ToString("D5") + ".ppm" + "\t" +
            Math.Round(screenPos.x) + "\t" + Math.Round(screenPos.y) + "\t" +
            Math.Round(camAngle.x) + "\t" + Math.Round(camAngle.y) + "\t" +
            Math.Round(camAngle.z);
                        // + "\t" + bi_gaze_dir.z
                        // + "\t" + right_gaze_dir.x + "\t" + right_gaze_dir.y
                        // + "\t" + right_gaze_dir.z
                        // + "\t" + left_gaze_dir.x + "\t" + left_gaze_dir.y
                        // + "\t" + left_gaze_dir.z;

        sw.WriteLine(frame_log);

        ////////////////////////////////////////////////////////////////////////

        captureScreenshot |=
            frame_id >= 230  & // don't capture screenshots when video is not playing; 250 frames black (50fps * 5 sekunde) - 20 = 3 black screenshots, frame 250 last black
            frame_id % frame_rate == 0 & // capture every nth screenshot
            frame_id <= video_frame_max-60; // don't capture screenshots after last frame;  100 frames black (50fps * 2 sekunde) - 60 = 4 black screenshots, frame 1000 last color

        if (captureScreenshot)

        {

            Debug.Log("Take screen shot: " + frame_id.ToString("D5"));

            // reset status for next evaluation
            captureScreenshot = false;

            // create screenshot objects if needed
            if (renderTexture == null)
            {
                // creates off-screen render texture that can rendered into
                rect = new Rect(0, 0, captureWidth, captureHeight);
                renderTexture = new RenderTexture(captureWidth, captureHeight, 24);
                screenShot = new Texture2D(captureWidth, captureHeight, TextureFormat.RGB24, false);
            }

            // get main camera and manually render scene into renderTexture
            Camera camera = this.GetComponent<Camera>(); // NOTE: added because there was no reference to camera in original script; must add this script to Camera
            camera.targetTexture = renderTexture;
            camera.Render();

            // read pixels will read from the currently active render texture so make our offscreen
            // render texture active and then read the pixels
            RenderTexture.active = renderTexture;
            screenShot.ReadPixels(rect, 0, 0);

            // reset active camera texture and render texture
            camera.targetTexture = null;
            RenderTexture.active = null;

            // get filename
            string screenshot_filename = frame_path + "/" + frame_id.ToString("D5") + ".ppm";

            // pull in our file header/data bytes for the specified image format (has to be done from main thread)
            byte[] screenshot_header = null;
            byte[] screenshot_data = null;

            // create a file header for ppm file
            string screenshot_headerstring = string.Format("P6\n{0} {1}\n255\n", rect.width, rect.height);
            screenshot_header = System.Text.Encoding.ASCII.GetBytes(screenshot_headerstring);
            screenshot_data = screenShot.GetRawTextureData();

            // create new thread to save the image to file (only operation that can be done in background)
            new System.Threading.Thread(() =>
            {
                // create file and write optional header with image bytes
                var f = System.IO.File.Create(screenshot_filename);
                if (screenshot_header != null) f.Write(screenshot_header, 0, screenshot_header.Length);
                f.Write(screenshot_data, 0, screenshot_data.Length);
                f.Close();
                //Debug.Log(string.Format("Wrote screenshot {0} of size {1}", screenshot_filename, screenshot_data.Length));
            }).Start();

            // cleanup if needed
            if (optimizeForManyScreenshots == false)
            {

                Destroy(renderTexture);
                renderTexture = null;
                screenShot = null;
            }
         }
        // sw.Close(); // needed?
    }


    // unused
    public void Kalibrieren()
    {
        //Kalibrieren
        smi.smi_ResetCalibration();
        smi.smi_StartFivePointCalibration();
        // smi.smi_StartNumericalValidation();
        // if (Input.GetKeyDown(KeyCode.Space)) { smi.smi_StartFivePointCalibration(); } // maybe easiest to just start calibration by hand!
        // if (Input.GetKeyDown(KeyCode.V)) { smi.smi_StartNumericalValidation(); }
        // if (Input.GetKeyDown(KeyCode.T)) { Time.timeScale = 1; counting = true; HideView.SetActive(false); }
        // if (counting) { counter++; }
    }

    public void EndWrite()
    {
        Debug.Log("Closing...");
        sw.Close();
    }

    ////////////////////////////////////////////////////////////////////////////
    private void UpdateThePosition()
        {

            gazeVis.SetActive(true);

            RaycastHit hitInformation;

            //Get raycast from gaze
            smiInstance.smi_GetRaycastHitFromGaze(out hitInformation);
            if (hitInformation.collider != null)
            {
                gazeVis.transform.position = hitInformation.point;
                gazeVis.transform.localRotation = smiInstance.transform.rotation;
                gazeVis.transform.localScale = initialeScale * hitInformation.distance;
                gazeVis.transform.LookAt(Camera.main.transform);
                gazeVis.transform.transform.rotation *= Quaternion.Euler(0, 180, 0);
            }
            else
            {
                //If the raycast does not collide with any object, put it far away.
                float distance = 100;
                Vector3 scale = initialeScale * distance;
                Ray gazeRay = smiInstance.smi_GetRayFromGaze();

                gazeVis.transform.position = gazeRay.origin + Vector3.Normalize(gazeRay.direction) * distance;
                gazeVis.transform.rotation = smiInstance.transform.rotation;
                if (gazeRay.direction != Vector3.zero)
                    gazeVis.transform.localScale = scale;
                else
                    gazeVis.transform.localScale = Vector3.zero;
            }

            //Toggle the gaze cursor by Key "g"
            if (Input.GetKeyDown(KeyCode.G))
            {
                gazeVis.GetComponent<MeshRenderer>().enabled = !(gazeVis.GetComponent<MeshRenderer>().enabled);
            }

        }

    // ? unused?
    void OnDisable()
    {
        EndWrite();
        Debug.Log("...done!");
    }
    ////////////////////////////////////////////////////////////////////////////
}
