Contains the raw data collected by the VR eye tracking device.


# Missing data

<table border="2" cellspacing="0" cellpadding="6" rules="groups" frame="hsides">


<colgroup>
<col  class="org-left" />

<col  class="org-left" />
</colgroup>
<thead>
<tr>
<th scope="col" class="org-left">missing</th>
<th scope="col" class="org-left">reason</th>
</tr>
</thead>

<tbody>
<tr>
<td class="org-left">02<sub>valpre</sub></td>
<td class="org-left">no log</td>
</tr>


<tr>
<td class="org-left">06<sub>rat</sub><sub>1</sub></td>
<td class="org-left">no log</td>
</tr>


<tr>
<td class="org-left">31<sub>rat</sub><sub>2</sub></td>
<td class="org-left">no log</td>
</tr>


<tr>
<td class="org-left">34<sub>gas</sub><sub>2</sub></td>
<td class="org-left">no log</td>
</tr>


<tr>
<td class="org-left">39<sub>reg</sub><sub>1</sub></td>
<td class="org-left">no log</td>
</tr>


<tr>
<td class="org-left">39<sub>valpre</sub></td>
<td class="org-left">no log</td>
</tr>


<tr>
<td class="org-left">46<sub>rat</sub><sub>1</sub></td>
<td class="org-left">no log</td>
</tr>
</tbody>
</table>

