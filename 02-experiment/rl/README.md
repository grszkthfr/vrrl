
# Export instructions for BeGaze (SMI, SMI mobile eye tracker)

Order of the steps to perform is (for most steps) **not** optional, due to
changing entries for e.g. the *Export* menu


## 1. Drag folder **rl** into BeGaze&rsquo; start page

-   &ldquo;Drag folder here to create or update experiments.&rdquo;


## 2. *File* > *Modify experiment&#x2026;*


## 3. *Gaze Data & Participants*

-   for each participant fix subject<sub>location</sub>
-   rename each invalid entry to /b\_/subject<sub>location</sub>
-   **\*** indicates potential duplicate filename!

<table border="2" cellspacing="0" cellpadding="6" rules="groups" frame="hsides">


<colgroup>
<col  class="org-left" />

<col  class="org-left" />
</colgroup>
<thead>
<tr>
<th scope="col" class="org-left">old name</th>
<th scope="col" class="org-left">new name</th>
</tr>
</thead>

<tbody>
<tr>
<td class="org-left">1<sub>rat</sub> (1)</td>
<td class="org-left">1<sub>reg</sub></td>
</tr>


<tr>
<td class="org-left">2<sub>rat</sub> (1)</td>
<td class="org-left">2<sub>reg</sub></td>
</tr>


<tr>
<td class="org-left">9<sub>bac</sub> (1)</td>
<td class="org-left">b9<sub>bac</sub></td>
</tr>


<tr>
<td class="org-left">10<sub>gas</sub> (1)</td>
<td class="org-left">10<sub>rat</sub></td>
</tr>


<tr>
<td class="org-left">11<sub>reg</sub> (1)</td>
<td class="org-left">11<sub>bac</sub></td>
</tr>


<tr>
<td class="org-left">12<sub>valpost</sub></td>
<td class="org-left">12<sub>valpre</sub></td>
</tr>


<tr>
<td class="org-left">12<sub>gas</sub> (1)</td>
<td class="org-left">12<sub>gas</sub></td>
</tr>


<tr>
<td class="org-left">12<sub>valpost</sub> (1)</td>
<td class="org-left">12<sub>valpost</sub></td>
</tr>


<tr>
<td class="org-left">13<sub>rat</sub> (1)</td>
<td class="org-left">13<sub>rat</sub></td>
</tr>


<tr>
<td class="org-left">14<sub>valpost</sub></td>
<td class="org-left">14<sub>valpre</sub></td>
</tr>


<tr>
<td class="org-left">14<sub>valpre</sub></td>
<td class="org-left">14<sub>valpost</sub></td>
</tr>


<tr>
<td class="org-left">14<sub>gas</sub> (1)</td>
<td class="org-left">14<sub>gas</sub></td>
</tr>


<tr>
<td class="org-left">24<sub>gas</sub> (1)</td>
<td class="org-left">24<sub>loc</sub></td>
</tr>


<tr>
<td class="org-left">26<sub>loc</sub> (1)</td>
<td class="org-left">26<sub>loc</sub></td>
</tr>


<tr>
<td class="org-left">27<sub>valpost</sub></td>
<td class="org-left">27<sub>valpre</sub></td>
</tr>


<tr>
<td class="org-left">27<sub>valpost</sub> (1)</td>
<td class="org-left">27<sub>valpost</sub></td>
</tr>


<tr>
<td class="org-left">27<sub>bac</sub> (1)</td>
<td class="org-left">27<sub>bac</sub></td>
</tr>


<tr>
<td class="org-left">33<sub>rat</sub> (1)</td>
<td class="org-left">33<sub>rat</sub></td>
</tr>


<tr>
<td class="org-left">38<sub>reg</sub>(2)</td>
<td class="org-left">38<sub>reg</sub></td>
</tr>


<tr>
<td class="org-left">42<sub>valpost</sub> (1)</td>
<td class="org-left">b42<sub>valpost</sub></td>
</tr>
</tbody>
</table>


## 4. *File* > *Modify experiment&#x2026;*

-   Tab *Gaze Data*: Remove broken data


## 5. *Export* > *Metrics Export*

Needs to be done once for each experiment.

1.  *Select template* > *Favorites* > *complete<sub>smi</sub><sub>log</sub>*
2.  *Select Export Options*
    -   Set *Decimal places* to 0
    -   select *Write Header*
3.  *Export&#x2026;* > *03-data/01-preprocessing/exports<sub>rl</sub>/*
4.  check *per participant* as file becomes to big to split.


## 6. *Analysis* > *Gaze Replay*

Needs to be done for each participant (optional)

1.  Select participant
2.  *Export* > *Export Gaze Replay Video* **or** *Ctrl + v*
    1.  *Frames Per Second*: 25
        -   after preprocessing, somehow video gets exported with 10 FPS
    2.  *Encoder*: Performance [ffmpeg]
    3.  *Quality*: High
    4.  *Playback Speed*: 100%
    5.  uncheck *Apply watermark*
3.  uncheck *Export Stimulus Audio*
4.  not for validation videos
5.  check *Time Overlay*
6.  select *Timestamp*
7.  *Add to Queue*


## 7. *Export* > *Show Queue*

Needs to be done once for each experiment.

1.  *Settings&#x2026;*
    -   *Global Export Folder:* to **03-data/01-preprocessing/exports<sub>rl</sub>**
    -   *File naming scheme:* to *%participant%*
2.  *Start*

