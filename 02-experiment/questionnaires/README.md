-   [AQ-K, deutsch](https://econtent.hogrefe.com/doi/full/10.1026/1616-3443.36.4.280)
    -   [original](https://link.springer.com/article/10.1023%2FA%3A1005653411471)
    -   coded as 4 - 1
-   [SSQ](https://www.tandfonline.com/doi/abs/10.1207/s15327108ijap0303_3)
    -   coded as 1 - 4
-   [IPQ](https://www.mitpressjournals.org/doi/abs/10.1162/105474601300343603)
    -   coded as 1 - 7
-   [SIAS/D](https://psycnet.apa.org/record/2000-13226-004)
-   Demographics:
    -   f = 0, m = 1
    -   student: nein = 0, ja = 1
    -   education: 1 - 8
    -   impaired vision: no = 0, corrected = 1, not corrected = 2
    -   handedness: right = 1, left = 2, both = 3
    -   experience VR: 1 - 4

