% Options for packages loaded elsewhere
\PassOptionsToPackage{unicode}{hyperref}
\PassOptionsToPackage{hyphens}{url}
%
\documentclass[
]{article}
\usepackage{lmodern}
\usepackage{amssymb,amsmath}
\usepackage{ifxetex,ifluatex}
\ifnum 0\ifxetex 1\fi\ifluatex 1\fi=0 % if pdftex
  \usepackage[T1]{fontenc}
  \usepackage[utf8]{inputenc}
  \usepackage{textcomp} % provide euro and other symbols
\else % if luatex or xetex
  \usepackage{unicode-math}
  \defaultfontfeatures{Scale=MatchLowercase}
  \defaultfontfeatures[\rmfamily]{Ligatures=TeX,Scale=1}
\fi
% Use upquote if available, for straight quotes in verbatim environments
\IfFileExists{upquote.sty}{\usepackage{upquote}}{}
\IfFileExists{microtype.sty}{% use microtype if available
  \usepackage[]{microtype}
  \UseMicrotypeSet[protrusion]{basicmath} % disable protrusion for tt fonts
}{}
\makeatletter
\@ifundefined{KOMAClassName}{% if non-KOMA class
  \IfFileExists{parskip.sty}{%
    \usepackage{parskip}
  }{% else
    \setlength{\parindent}{0pt}
    \setlength{\parskip}{6pt plus 2pt minus 1pt}}
}{% if KOMA class
  \KOMAoptions{parskip=half}}
\makeatother
\usepackage{xcolor}
\IfFileExists{xurl.sty}{\usepackage{xurl}}{} % add URL line breaks if available
\IfFileExists{bookmark.sty}{\usepackage{bookmark}}{\usepackage{hyperref}}
\hypersetup{
  pdftitle={Reality in a sphere: A direct comparison of social attention in the laboratory and the real world},
  pdfauthor={Jonas D. Großekathöfer, Christian Seis, Matthias Gamer},
  hidelinks,
  pdfcreator={LaTeX via pandoc}}
\urlstyle{same} % disable monospaced font for URLs
\usepackage[margin=1in]{geometry}
\usepackage{longtable,booktabs}
% Correct order of tables after \paragraph or \subparagraph
\usepackage{etoolbox}
\makeatletter
\patchcmd\longtable{\par}{\if@noskipsec\mbox{}\fi\par}{}{}
\makeatother
% Allow footnotes in longtable head/foot
\IfFileExists{footnotehyper.sty}{\usepackage{footnotehyper}}{\usepackage{footnote}}
\makesavenoteenv{longtable}
\usepackage{graphicx}
\makeatletter
\def\maxwidth{\ifdim\Gin@nat@width>\linewidth\linewidth\else\Gin@nat@width\fi}
\def\maxheight{\ifdim\Gin@nat@height>\textheight\textheight\else\Gin@nat@height\fi}
\makeatother
% Scale images if necessary, so that they will not overflow the page
% margins by default, and it is still possible to overwrite the defaults
% using explicit options in \includegraphics[width, height, ...]{}
\setkeys{Gin}{width=\maxwidth,height=\maxheight,keepaspectratio}
% Set default figure placement to htbp
\makeatletter
\def\fps@figure{htbp}
\makeatother
\setlength{\emergencystretch}{3em} % prevent overfull lines
\providecommand{\tightlist}{%
  \setlength{\itemsep}{0pt}\setlength{\parskip}{0pt}}
\setcounter{secnumdepth}{5}
\usepackage{threeparttable}
\usepackage{booktabs}
\usepackage{float} \floatplacement{figure}{H}
\newcommand{\beginsupplement}{\setcounter{table}{0}  \renewcommand{\thetable}{S\arabic{table}} \setcounter{figure}{0} \renewcommand{\thefigure}{S\arabic{figure}}}
\newlength{\cslhangindent}
\setlength{\cslhangindent}{1.5em}
\newenvironment{cslreferences}%
  {\setlength{\parindent}{0pt}%
  \everypar{\setlength{\hangindent}{\cslhangindent}}\ignorespaces}%
  {\par}

\title{Reality in a sphere: A direct comparison of social attention in the laboratory and the real world}
\usepackage{etoolbox}
\makeatletter
\providecommand{\subtitle}[1]{% add subtitle to \maketitle
  \apptocmd{\@title}{\par {\large #1 \par}}{}{}
}
\makeatother
\subtitle{Supplementary Material}
\author{Jonas D. Großekathöfer, Christian Seis, Matthias Gamer}
\date{}

\begin{document}
\maketitle

\setcounter{table}{0}  \renewcommand{\thetable}{S\arabic{table}} \setcounter{figure}{0} \renewcommand{\thefigure}{S\arabic{figure}}

\hypertarget{assumption-check-robust-lmm}{%
\section{Assumption check \& robust LMM}\label{assumption-check-robust-lmm}}

A central assumption for the preregistered linear mixed models was not met by
our data: the residuals were not normally distributed (see Figure S1,
\(W = 0.94\), \(p < .001\)).

\begin{figure}
\centering
\includegraphics{supplement_vrrl_files/figure-latex/qqplot-1.pdf}
\caption{\label{fig:qqplot}QQ plot for the residuals of the originally preregistered model.}
\end{figure}

Since this can have severe consequences for the statistical properties of the
linear mixed model (Kenny and Judd 1986), we additionally calculated a robust linear
mixed model following Koller (2016). These methods aim to provide robust
estimates in conditions where assumptions for linear mixed models are violated.
For the current study, the estimated coefficients of the robust model (see Table
S1) closely matched the coefficients of the original linear mixed model (see
Table 1 in the main article). As a result, the estimates, standard errors, and
t-values are very similar and support our interpretation of the original
findings in the main article.

\begin{table}[tbp]

\begin{center}
\begin{threeparttable}

\caption{\label{tab:rob-lmm-1-coef}Estimated coefficients for a robust linear mixed model (Koller, 2016), specified exactly like the preregistered model 1 (see main article) with environment and ROI as fixed and participant ID as random effects for the prediction of gaze proportions.}

\begin{tabular}{llll}
\toprule
 & \multicolumn{1}{c}{Estimate} & \multicolumn{1}{c}{$SE$} & \multicolumn{1}{c}{$t$}\\
\midrule
Intercept & 0.19 & 0.00 & 38.03\\
Environment (RE) & -0.09 & 0.00 & -18.63\\
ROI (object) & 0.02 & 0.00 & 3.64\\
Environment (RE) × ROI (object) & 0.02 & 0.00 & 3.09\\
\bottomrule
\addlinespace
\end{tabular}

\begin{tablenotes}[para]
\normalsize{\textit{Note.} The robust linear mixed model is based on sum-to-zero contrasts. RE: real environment, ROI: region of interest.}
\end{tablenotes}

\end{threeparttable}
\end{center}

\end{table}

\hypertarget{convergence-issues-in-model-4}{%
\section{Convergence issues in Model 4}\label{convergence-issues-in-model-4}}

Following the outlined model building path in the manuscript the complete model
was initially specified\footnote{Using the \texttt{lme4} notation (Bates et al. 2015).} as:

\[
fix \sim env * roi * P + ( 1 | sub ) + ( 1 + P | loc )
\]

The correlation between location and present persons was estimated to be exactly
-1 (see Table S2). Thus, the estimated correlation can be considered
compromised, resulting in meaningless model output. In a subsequent step, we,
therefore, suppressed the correlation between the random slope for present
persons and locations, and specified the following pruned model (note the \texttt{\textbar{}\textbar{}}
indicating uncorrelated effects in the random term for \texttt{loc}):

\[
fix \sim env * roi * P + ( 1 | sub ) + ( 1 + P || loc )
\]

The model converged successfully and is reported in the manuscript (see Table 5).

\begin{table}[tbp]

\begin{center}
\begin{threeparttable}

\caption{\label{tab:sing-lmm-pb-varcor}Estimated standard deviations, and correlations between the random-effects.}

\begin{tabular}{llll}
\toprule
Groups & \multicolumn{1}{c}{Name} & \multicolumn{1}{c}{SD} & \multicolumn{1}{c}{$r$}\\
\midrule
Subject (sub) & Intercept & 0.03 & \\
Location (loc) & Intercept & 0.02 & \\
 & Present persons (P) & 0.01 & -1\\
Residual &  & 0.13 & \\
\bottomrule
\end{tabular}

\end{threeparttable}
\end{center}

\end{table}

\newpage

\hypertarget{references}{%
\section{References}\label{references}}

\begingroup
\setlength{\parindent}{-0.5in}
\setlength{\leftskip}{0.5in}

\hypertarget{refs}{}
\begin{cslreferences}
\leavevmode\hypertarget{ref-R-lme4}{}%
Bates, Douglas, Martin Mächler, Ben Bolker, and Steve Walker. 2015. ``Fitting Linear Mixed-Effects Models Using lme4.'' \emph{Journal of Statistical Software} 67 (1): 1--48. \url{https://doi.org/10.18637/jss.v067.i01}.

\leavevmode\hypertarget{ref-kenny1986}{}%
Kenny, David A., and Charles M. Judd. 1986. ``Consequences of Violating the Independence Assumption in Analysis of Variance.'' \emph{Psychological Bulletin} 99 (3): 422--31. \url{https://doi.org/10.1037/0033-2909.99.3.422}.

\leavevmode\hypertarget{ref-R-robustlmm}{}%
Koller, Manuel. 2016. ``robustlmm: An R Package for Robust Estimation of Linear Mixed-Effects Models.'' \emph{Journal of Statistical Software} 75 (6): 1--24. \url{https://doi.org/10.18637/jss.v075.i06}.
\end{cslreferences}

\endgroup

\end{document}
